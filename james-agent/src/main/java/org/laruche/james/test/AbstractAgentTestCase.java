package org.laruche.james.test;

import jade.core.Agent;
import org.laruche.james.agent.JadeMessageHandler;
import org.laruche.james.plugin.MainAgentPlugin;
import org.laruche.james.test.agent.TestManagerAgent;
import org.laruche.james.test.bean.TestResult;

import java.io.Serializable;

import static java.lang.Thread.sleep;
import static org.laruche.james.plugin.MainAgentPlugin.createMainAgentPlugin;

/**
 * <p>
 * Classe bastraite utilisée afin d'implémenter des tests unitaires permettant de
 * tester les agents et leurs fonctionnalités.
 * </p>
 *
 * @param <T> : type du retour des agents, implémentant l'interface Serializable.
 * @see java.io.Serializable
 */
public abstract class AbstractAgentTestCase<T extends Serializable> implements JadeMessageHandler {
    public static final int WAITING_TIME = 3000;
    public static final String TEST_MANAGER_AGENT_NAME = "testManagerAgent";
    public static final String AGENT_PLUGIN_NAME = "agentPlugin";
    protected MainAgentPlugin mainAgentPlugin;
    protected TestManagerAgent<T> testManagerAgent;

    /// // Constructeur(s)

    public AbstractAgentTestCase() {
        this.mainAgentPlugin = createMainAgentPlugin(AGENT_PLUGIN_NAME);
        this.testManagerAgent = new TestManagerAgent<>();
    }

    /// // Méthode(s) générale(s)

    /**
     * Méthode d'initialisation par défaut. <br />
     *
     * @throws Exception :  En cas de problème
     */
    protected void setUp() throws Exception {
        this.mainAgentPlugin.addAgentToStart(TEST_MANAGER_AGENT_NAME, testManagerAgent);
    }

    protected void tearDown() throws Exception {
        if (mainAgentPlugin == null) {
            return;
        }
        if (mainAgentPlugin.isStarted()) {
            this.mainAgentPlugin.close();
        }
        mainAgentPlugin.clearAgents();
    }

    protected void startAgentPlugin() throws Exception {
        this.mainAgentPlugin.start();
        sleep(WAITING_TIME);
    }

    /// /// Getters & Setters :

    /**
     * Méthode permettant de retourner l"agent sous-jacent utilisé
     * pour la gestion des messages c'est à dire l'instance TestManagerAgent. <br />
     *
     * @return agent de test.
     * @see TestManagerAgent
     */
    @Override
    public Agent getAgent() {
        return this.testManagerAgent;
    }

    protected TestResult<T> getTestResult() {
        return this.testManagerAgent.getTestResult();
    }

    protected void setTestResult(final TestResult<T> testResult) {
        this.testManagerAgent.setTestResult(testResult);
    }


}
