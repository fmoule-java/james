package org.laruche.james.test.agent;

import org.laruche.james.agent.AbstractAgent;
import org.laruche.james.agent.behavior.AbstractHandlingMessageBehavior;
import org.laruche.james.agent.behavior.AbstractHandlingMessageBehavior.HandlingMessageMode;
import org.laruche.james.agent.behavior.SequentialHandlingMessageBehavior;
import org.laruche.james.test.bean.TestResult;
import org.laruche.james.test.behavior.GetSimpleResponseMessageBehavior;
import org.laruche.james.test.behavior.ShowErrorBehavior;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import static jade.lang.acl.ACLMessage.*;
import static jade.lang.acl.MessageTemplate.MatchPerformative;
import static java.util.Objects.requireNonNull;
import static org.laruche.james.agent.behavior.AbstractHandlingMessageBehavior.HandlingMessageMode.STRING;

/**
 * Agent permettant d'interroger les autres agents lors des tests unitaires. <br />
 */
public class TestManagerAgent<U extends Serializable> extends AbstractAgent {
    private TestResult<U> testResult = new TestResult<>();
    private final Map<Integer, SequentialHandlingMessageBehavior> msgHandlingBehaviour = new HashMap<>();
    private HandlingMessageMode messageMode;

    /// // Constructors :

    public TestManagerAgent(final HandlingMessageMode handlingMessageMode) {
        super();
        this.messageMode = handlingMessageMode;
        initBehaviors();
    }

    public TestManagerAgent() {
        this(STRING);
    }

    /// // Méthodes générales

    protected void initBehaviors() {
        final GetSimpleResponseMessageBehavior<U> getSimpleResponseMessageBehavior = new GetSimpleResponseMessageBehavior<>(messageMode, testResult);
        this.addHandlingMessageBehaviour(FAILURE, new ShowErrorBehavior<>(testResult));
        this.addHandlingMessageBehaviour(INFORM, getSimpleResponseMessageBehavior);
        this.addHandlingMessageBehaviour(REQUEST, getSimpleResponseMessageBehavior);
    }

    protected void addHandlingMessageBehaviour(final Integer performative, final AbstractHandlingMessageBehavior handlingMessageBehavior) {
        requireNonNull(performative, "Le paramètre 'performative' ne doit pas être nul");
        requireNonNull(handlingMessageBehavior, "Le paramètre 'handlingMessageBehavior' ne doit pas être nul");
        final SequentialHandlingMessageBehavior seqBehaviour;
        if (!msgHandlingBehaviour.containsKey(performative)) {
            seqBehaviour = new SequentialHandlingMessageBehavior(MatchPerformative(performative));
            this.addBehaviour(seqBehaviour);
            msgHandlingBehaviour.put(performative, seqBehaviour);
        } else {
            seqBehaviour = msgHandlingBehaviour.get(performative);
        }
        seqBehaviour.addSubBehaviour(handlingMessageBehavior);
    }

    /// // Getters & Setters :

    public TestResult<U> getTestResult() {
        return testResult;
    }

    public void setTestResult(final TestResult<U> testResult) {
        this.testResult = testResult;
    }

    public void setHandlingMessageMode(final HandlingMessageMode messageMode) {
        this.messageMode = messageMode;
    }
}
