package org.laruche.james.test.bean;

import static org.laruche.james.test.bean.TestResult.TestResultStatus.ERROR;
import static org.laruche.james.test.bean.TestResult.TestResultStatus.OK;

/**
 * Objet JAVA représentant le résultat d'un test unitaire. <br />
 *
 * @param <V> : type de la valeur du test.
 */
public class TestResult<V> {
    private static final String EMPTY_STRING = "";
    private TestResultStatus status = OK;
    private String errorMessage = EMPTY_STRING;
    private V value;

    /// // Constructeur(s) :

    public TestResult(final V value) {
        this.value = value;
    }

    public TestResult() {
        this(null);
    }

    /// // Méthode(s) de la classe Object

    @Override
    public String toString() {
        final StringBuilder buffer = new StringBuilder();
        buffer.append("TestResult [");
        buffer.append("status=");
        if (this.isInError()) {
            buffer.append("ERROR");
        } else {
            buffer.append("OK");
        }
        buffer.append(",value=");
        if (this.value != null) {
            buffer.append(this.value);
        } else {
            buffer.append("null");
        }
        buffer.append("]");
        return buffer.toString();
    }

    /// // Getters & Setters :

    public V getValue() {
        return value;
    }

    public void setValue(final V value) {
        this.value = value;
    }

    public boolean isInError() {
        return this.status == ERROR;
    }

    public String getErrorMessage() {
        return this.errorMessage;
    }

    public void setErrorMessage(final String errorMessage) {
        this.status = ERROR;
        this.errorMessage = errorMessage;
    }

    /// // Classe(s) interne(s) :

    /**
     * Statut du résultat du test. <br />
     */
    public enum TestResultStatus {
        OK,
        ERROR
    }
}
