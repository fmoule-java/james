package org.laruche.james.test.behavior;

import jade.lang.acl.ACLMessage;
import org.laruche.james.agent.behavior.AbstractHandlingMessageBehavior;
import org.laruche.james.test.bean.TestResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

import static jade.lang.acl.ACLMessage.FAILURE;
import static jade.lang.acl.MessageTemplate.MatchPerformative;

/// // Behavior :

public class ShowErrorBehavior<U extends Serializable> extends AbstractHandlingMessageBehavior {
    private static final Logger LOGGER = LoggerFactory.getLogger(ShowErrorBehavior.class);
    private final TestResult<U> testResult;

    public ShowErrorBehavior(final TestResult<U> testResult) {
        super(MatchPerformative(FAILURE));
        this.testResult = testResult;
    }

    @Override
    public void doAction(final ACLMessage errorMessage) {
        try {
            if (errorMessage != null) {
                LOGGER.error("ERROR : {}", this.getResponseContentAsString(errorMessage));
                this.testResult.setErrorMessage("ERROR : " + (this.getResponseContentAsString(errorMessage)));
                this.testResult.setValue(null);
            }
        } catch (final Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
            this.testResult.setErrorMessage(exception.getMessage());
        }
    }
}
