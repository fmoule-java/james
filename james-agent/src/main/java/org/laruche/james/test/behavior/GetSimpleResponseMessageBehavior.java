package org.laruche.james.test.behavior;

import jade.lang.acl.ACLMessage;
import org.laruche.james.agent.behavior.AbstractHandlingMessageBehavior;
import org.laruche.james.test.bean.TestResult;

import java.io.Serializable;

import static jade.lang.acl.ACLMessage.INFORM;
import static jade.lang.acl.ACLMessage.REQUEST;
import static jade.lang.acl.MessageTemplate.MatchPerformative;
import static jade.lang.acl.MessageTemplate.or;
import static java.util.Objects.requireNonNull;

public class GetSimpleResponseMessageBehavior<U extends Serializable> extends AbstractHandlingMessageBehavior {
    private final TestResult<U> testResult;

    /// // Constructeur(s)

    /**
     * Constructeur avec le mode et l'instance <i>TestResult</i>
     *
     * @param handlingMessageMode : mode
     * @param testResult          : instance de TestResult
     */
    public GetSimpleResponseMessageBehavior(final HandlingMessageMode handlingMessageMode,
                                            final TestResult<U> testResult) {
        super(handlingMessageMode, or(MatchPerformative(INFORM), MatchPerformative(REQUEST)));
        this.testResult = requireNonNull(testResult);
    }

    /// // Méthodes de la classe AbstractHandlingMessageBehavior

    @Override
    public void doAction(final ACLMessage respMessage) {
        try {
            if (respMessage == null) {
                throw new Exception("La réponse du message est nul !!");
            }
            final U responseContent = getResponseContent(respMessage);
            this.getLogger().debug("----> GetSimpleResponseMessageBehavior : Récupération de testResult = {}", responseContent);
            this.testResult.setValue(responseContent);
        } catch (final ClassCastException classCastException) {
            this.testResult.setErrorMessage("Le contenu du message n'a pas le bon type");
        } catch (final Exception exception) {
            this.testResult.setErrorMessage(exception.getMessage());
        }
    }

}
