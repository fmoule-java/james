package org.laruche.james.agent;

import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.onto.Ontology;
import jade.core.Agent;
import jade.core.behaviours.Behaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import org.laruche.james.message.HasOntology;
import org.laruche.james.plugin.HasPluginMananger;
import org.laruche.james.plugin.PluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.util.Arrays.asList;
import static org.laruche.james.agent.AgentUtils.createServiceDescription;
import static org.laruche.james.message.MessageUtils.DEFAULT_LANGUAGE;

/**
 * <p>
 * Classe abstraite représentant les agents et les méthodes
 * communes. <br />
 * </p>
 *
 * @author Frédéric Moulé
 */
public abstract class AbstractAgent extends Agent implements JadeMessageHandler, HasOntology, HasPluginMananger {
    public static final ServiceDescription DEFAULT_SERVICE_DESCRIPTION = createServiceDescription("default-service", "default-agent");
    private static final Logger DEFAULT_LOGGER = LoggerFactory.getLogger(AbstractAgent.class);
    private final Set<Behaviour> closeableBehaviours = new HashSet<>();
    private final Set<ServiceDescription> serviceDescriptions = new HashSet<>();
    private final Codec agentLanguage;
    public PluginManager pluginManager;
    private Logger logger;
    protected final Set<Ontology> ontologies;
    private transient boolean started;
    private transient boolean registred;
    private final transient ReentrantReadWriteLock lock;

    /// // Constructeur(s)

    protected AbstractAgent() {
        this.started = false;
        this.registred = false;
        this.ontologies = new HashSet<>();
        this.logger = LoggerFactory.getLogger(AbstractAgent.class);
        this.lock = new ReentrantReadWriteLock();
        this.agentLanguage = DEFAULT_LANGUAGE;

        // Ajout de la description par défaut
        this.addServiceDescription(DEFAULT_SERVICE_DESCRIPTION);
    }

    /// // Méthodes de la classe Object :

    @Override
    public int hashCode() {
        return Objects.hashCode(this.getAID());
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (this.getClass() != obj.getClass())) {
            return false;
        }
        return (this.getAID().equals(((AbstractAgent) obj).getAID()));
    }

    /// // Méthode(s) de l'interface JadeMessageHandler

    @Override
    public Agent getAgent() {
        return this;
    }

    /// // Méthode de la classe Agent :

    @Override
    protected void setup() {
        lock.writeLock().lock();
        try {
            this.getContentManager().registerLanguage(getAgentLanguage());
            this.doSetUp();
            this.started = true;
        } catch (final Exception e) {
            logger.error(e.getMessage(), e);
            this.doDelete();
            this.started = false;
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Démarrage de l'agent
     */
    protected void doSetUp() throws Exception {
        if (!serviceDescriptions.isEmpty()) {
            DFService.register(this, this.getAgentDescription());
            this.registred = true;
        }
    }


    /**
     * Définition de l'arrêt de l'agent.
     */
    @Override
    protected void takeDown() {
        lock.writeLock().lock();
        try {
            doTakeDown();
            if (registred) {
                DFService.deregister(this);
                this.registred = false;
            }
            this.started = false;
        } catch (final Exception e) {
            logger.error(e.getMessage(), e);
        } finally {
            lock.writeLock().unlock();
        }
    }

    /**
     * Arrêt de l'agent
     */
    protected void doTakeDown() throws Exception {
        for (Behaviour closeableBehaviour : closeableBehaviours) {
            if (closeableBehaviour instanceof AutoCloseable) {
                ((AutoCloseable) closeableBehaviour).close();
            }
        }
    }

    /**
     * Retourne le codec de l'instance. <br />
     *
     * @return codec utilisé par l'agent
     * @see Codec
     */
    protected Codec getAgentLanguage() {
        return agentLanguage;
    }

    /// // Méthodes générales :

    @Override
    public void addBehaviour(final Behaviour behaviour) {
        if (behaviour instanceof AutoCloseable) {
            closeableBehaviours.add(behaviour);
        }
        if (behaviour instanceof HasOntology ontologyBehavior) {
            ontologyBehavior.addOntologies(ontologies);
        }
        super.addBehaviour(behaviour);
    }

    /**
     * Méthode permettant d'ajouter une description à l'agent. <br />
     *
     * @param serviceDescription : Description du service proposé par l'agent.
     */
    public void addServiceDescription(final ServiceDescription serviceDescription) {
        if (serviceDescription == null) {
            return;
        }
        serviceDescriptions.remove(DEFAULT_SERVICE_DESCRIPTION);
        serviceDescriptions.add(serviceDescription);
    }

    /**
     * <p>
     * Méthode permettant d'ajouter une ontologie gérée par l'agent
     * dans l'utlisation des messages. <br />
     * </p>
     *
     * @param ontology : ontologie ajoutée.
     */
    public void addOntology(final Ontology ontology) {
        if (ontology == null) {
            return;
        }
        this.ontologies.add(ontology);
        final ContentManager contentManager = this.getContentManager();
        if (contentManager != null) {
            contentManager.registerOntology(ontology);
        }
    }

    ///// Getters & Setters /////

    /**
     * Méthode montrant si l'agent est démarré
     *
     * @return démarré ou non
     */
    public boolean isStarted() {
        lock.readLock().lock();
        try {
            return started;
        } finally {
            lock.readLock().unlock();
        }
    }

    /**
     * <p>
     * Méthode retournant la description de l'agent
     * utilisée par la DF de la plateforme.<br />
     * </p>
     *
     * @return description de l'agent
     * @see DFAgentDescription
     */
    public DFAgentDescription getAgentDescription() {
        final DFAgentDescription description = new DFAgentDescription();
        description.setName(this.getAID());
        this.serviceDescriptions.forEach(description::addServices);
        this.ontologies.stream().map(Ontology::getName).forEach(description::addOntologies);
        description.addLanguages(this.agentLanguage.getName());
        return description;
    }


    /**
     * Méthode permettant de récupérer les arguments sous la forme
     * de la liste. <br />
     *
     * @return liste des arguments
     */
    protected List<Object> getArgumentsAsList() {
        final Object[] arguments = this.getArguments();
        if (arguments == null || arguments.length == 0) {
            return new ArrayList<>();
        }
        return asList(arguments);
    }

    public PluginManager getPluginManager() {
        return pluginManager;
    }

    public void setPluginManager(final PluginManager pluginManager) {
        this.pluginManager = pluginManager;
    }

    public Set<Ontology> getOntologies() {
        return ontologies;
    }

    /**
     * <p>
     * Méthode retournant le logger s'il est non nul. Dans le cas contraiire
     * on renvoie le logger par défaut.
     * </p>
     *
     * @return instance du logger
     * @see Logger
     */
    public Logger getLogger() {
        return (logger == null ? DEFAULT_LOGGER : logger);
    }

    /**
     * Méthode permettant de spécifier le "logger" utilisé
     * par l'agent. <br />
     *
     * @param logger : logger
     */
    public void setLogger(final Logger logger) {
        this.logger = logger;
    }
}