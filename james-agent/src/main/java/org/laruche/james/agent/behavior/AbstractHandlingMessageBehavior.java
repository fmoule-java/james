package org.laruche.james.agent.behavior;

import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.io.Serializable;

import static org.laruche.james.agent.behavior.AbstractHandlingMessageBehavior.HandlingMessageMode.STRING;

/**
 * <p>
 * Comportement de base permettant de réagir à des messages.
 * </p>
 *
 * @author Frédéric Moulé
 * @see AbstractBehavior
 */
public abstract class AbstractHandlingMessageBehavior extends AbstractBehavior {
    private final MessageTemplate msgPredicate;
    protected final HandlingMessageMode handlingMessageMode;

    /// // Constructeurs :

    protected AbstractHandlingMessageBehavior(final HandlingMessageMode handlingMessageMode, final MessageTemplate msgPredicate) {
        this.msgPredicate = msgPredicate;
        this.handlingMessageMode = handlingMessageMode;
    }

    protected AbstractHandlingMessageBehavior(final MessageTemplate msgPredicate) {
        this(STRING, msgPredicate);
    }

    protected AbstractHandlingMessageBehavior() {
        this(null);
    }

    @Override
    public void action() {
        final ACLMessage message;
        if (msgPredicate == null) {
            message = receiveMessage();
        } else {
            message = receiveMessage(msgPredicate);
        }
        if (message != null) {
            doAction(message);
            this.block();
        } else {
            this.block();
        }
    }

    /**
     * Méthode définissant l'action de l'agent en fonction du message. <br />
     *
     * @param message : message à traiter
     */
    public abstract void doAction(final ACLMessage message);

    /// // Getters & Setters

    @SuppressWarnings("unchecked")
    protected <T extends Serializable> T getResponseContent(final ACLMessage respMessage) throws Exception {
        if (this.handlingMessageMode == STRING) {
            return (T) respMessage.getContent();
        } else {
            return (T) respMessage.getContentObject();
        }
    }

    public String getResponseContentAsString(final ACLMessage respMessage) throws Exception {
        final Serializable responseContent = this.getResponseContent(respMessage);
        if (responseContent == null) {
            return "";
        }
        return responseContent.toString();
    }


    /// // Classe(s) interne(s) :


    /**
     * <p>
     * Mode de gestion des messages
     * </p>
     *
     * @author Frédéric Moulé
     */
    public enum HandlingMessageMode {
        STRING,
        OBJECT;
    }

}
