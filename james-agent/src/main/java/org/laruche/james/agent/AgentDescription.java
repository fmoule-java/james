package org.laruche.james.agent;

import jade.core.AID;

import java.util.Objects;

/**
 * Classe représentant les descriptions des agents. <br />
 *
 * @author Frédéric Moulé
 */
public class AgentDescription {
    private final AID agentId;
    private final String plateformId;

    /// // Constructeur(s)

    /**
     * Constructeur de base
     *
     * @param agentId     :  identifiant
     * @param plateformId : identifiant du conteneur correspondant l'instance de AgentPlugin
     * @see org.laruche.james.plugin.AbstractAgentPlugin
     */
    public AgentDescription(final AID agentId, final String plateformId) {
        this.agentId = agentId;
        this.plateformId = plateformId;
    }

    /// // Méthode(s) de la classe Object :

    @Override
    public int hashCode() {
        return Objects.hashCode(agentId);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || this.getClass() != obj.getClass()) {
            return false;
        }
        final AgentDescription that = (AgentDescription) obj;
        return Objects.equals(this.agentId, that.agentId);
    }

    /// // Getters & Setters :

    /**
     * Retourne l'identifiant JADE de l'agent. <br />
     *
     * @return identifiant JADE de l'agent
     */
    public AID getAgentId() {
        return agentId;
    }

    /**
     * Retourne l'identifiant de la plateforme
     *
     * @return identifiant de la plateforme
     */
    public String getPlateformId() {
        return plateformId;
    }

    public String getAgentName() {
        return (this.agentId == null ? null : this.agentId.getLocalName());
    }
}
