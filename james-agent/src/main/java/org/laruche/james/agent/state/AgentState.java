package org.laruche.james.agent.state;

import java.util.Objects;

import static jade.core.Agent.*;
import static jade.core.AgentState.getInstance;

/**
 * <p>
 * Enumération décrivant l'état des agents.<br />
 * De façon plus précise, cette classe encapsule la classe <b>AgentState</b> du framwork JADE
 * </p>
 *
 * @author Frédéric Moulé
 * @see jade.core.AgentState
 */
public enum AgentState {
    INITIATED(AP_INITIATED),
    IDLE(AP_IDLE),
    ACTIVE(AP_ACTIVE),
    SUSPENDED(AP_SUSPENDED),
    WAITING(AP_WAITING),
    DELETED(AP_DELETED),
    UNKNWON(null);

    private final jade.core.AgentState jadeState;

    /// // Constructeur(s)

    AgentState(final Integer stateCode) {
        this.jadeState = (stateCode == null ? null : getInstance(stateCode));
    }

    /// // Méthode(s) générale(s)

    /**
     * Méthode permettant de savoir si l'instance courante
     * correspond à un état actif
     *
     * @return booléen correspondant au résultat
     */
    public boolean isActive() {
        boolean isActive = this == ACTIVE;
        isActive = isActive || (this == WAITING);
        return isActive;
    }

    /// // Méthode(s) statique(s) :

    /**
     * <p>
     * Permet de construire une instance de l'enumération AgentState
     * à partir d'une instance jade.core.AgentState
     * </p>
     *
     * @param jadeState : instance de la classe jade.core.AgentState
     * @return instance de l'énumération AgentState.
     */
    public static AgentState fromJadeAgentState(final jade.core.AgentState jadeState) {
        if (jadeState == null) {
            return UNKNWON;
        }
        for (AgentState agentState : AgentState.values()) {
            if (!Objects.equals(jadeState, agentState.jadeState)) {
                continue;
            }
            return agentState;
        }
        return UNKNWON;
    }

    public static AgentState fromJadeAgentStateCode(final Integer stateCode) {
        if (stateCode == null) {
            return UNKNWON;
        }
        for (AgentState agentState : AgentState.values()) {
            if (agentState.jadeState == null || !Objects.equals(stateCode, agentState.jadeState.getValue())) {
                continue;
            }
            return agentState;
        }
        return UNKNWON;
    }

}
