package org.laruche.james.agent.javafx;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.URL;

import static java.lang.Thread.sleep;

/// // Classe(s) interne(s)

public class JavaFXApplication extends Application {
    public static final String INIT_FXML_FILE_NAME = "mainGUI.fxml";
    private static JavaFXApplication instance;
    private transient Scene scene;

    public JavaFXApplication() {
        this.scene = null;
        instance = this;
    }

    /// // Méthodes de la classe Application :

    @Override
    public void start(final Stage stage) throws Exception {
        final File initFxmlFile = getInitFxmlFile();
        if (initFxmlFile == null) {
            throw new Exception("Le fichier FXML d'initialisation est nul!");
        } else if (!initFxmlFile.exists()) {
            throw new FileNotFoundException(initFxmlFile.getAbsolutePath());
        }
        if (scene == null) {
            scene = new Scene(JavaFXAgent.loadFXML(initFxmlFile).getLeft(), 640, 480);
        }
        stage.setScene(scene);
        stage.show();
    }

    public File getInitFxmlFile() throws FileNotFoundException {
        final URL initFileRessource = this.getClass().getClassLoader().getResource(INIT_FXML_FILE_NAME);
        if (initFileRessource == null) {
            throw new FileNotFoundException("Le fichier %s n'a pas été trouvé".formatted(INIT_FXML_FILE_NAME));
        }
        return new File(initFileRessource.getPath());
    }

    public void redirectTo(final File fxmlFile) throws Exception {
        this.scene.setRoot(JavaFXAgent.loadFXML(fxmlFile).getLeft());
    }

    /// // Méthode(s) statique(s) :

    public synchronized static JavaFXApplication getInstance() throws Exception {
        if (instance == null) {
            Thread.startVirtualThread(() -> Application.launch(JavaFXApplication.class));
            while (instance == null) {
                //noinspection BusyWait
                sleep(100L);
            }
        }
        return instance;
    }

    /// // Getters & Setters :

    public Scene getScene() {
        return scene;
    }
}
