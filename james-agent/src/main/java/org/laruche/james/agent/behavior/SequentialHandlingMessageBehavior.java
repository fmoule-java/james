package org.laruche.james.agent.behavior;

import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * <p>
 * Ce comportement d'agent permet d'exécuter séquentiellement des taches avec un message reçu. <br />
 *
 * @see jade.core.behaviours.SequentialBehaviour
 * </p>
 */
public class SequentialHandlingMessageBehavior extends AbstractHandlingMessageBehavior {
    private final List<AbstractHandlingMessageBehavior> subBehaviours = new ArrayList<>();
    private final transient ReentrantReadWriteLock lock;

    ///// Constructeur(s) :

    public SequentialHandlingMessageBehavior() {
        super();
        this.lock = new ReentrantReadWriteLock();
    }

    public SequentialHandlingMessageBehavior(final MessageTemplate msgPredicate) {
        super(msgPredicate);
        this.lock = new ReentrantReadWriteLock();
    }

    @Override
    public void doAction(final ACLMessage message) {
        this.lock.readLock().lock();
        try {
            for (AbstractHandlingMessageBehavior subBehaviour : subBehaviours) {
                subBehaviour.doAction(message);
            }
        } finally {
            this.lock.readLock().unlock();
        }
    }

    /**
     * Permet d'ajouter un comportement à exécuter avec le message. <br />
     *
     * @param handlingMessageBehavior : comportement
     */
    public void addSubBehaviour(final AbstractHandlingMessageBehavior handlingMessageBehavior) {
        this.lock.writeLock().lock();
        try {
            this.subBehaviours.add(handlingMessageBehavior);
        } finally {
            this.lock.writeLock().unlock();
        }
    }
}
