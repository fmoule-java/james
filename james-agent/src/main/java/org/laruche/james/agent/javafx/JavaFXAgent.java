package org.laruche.james.agent.javafx;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.util.Callback;
import org.apache.commons.lang3.tuple.Pair;
import org.laruche.james.agent.AbstractAgent;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;

import static org.laruche.james.agent.AgentUtils.createServiceDescription;

/**
 * <p>
 * Agent de base permettant de lancer une interface
 * JavaFX.
 * </p>
 *
 * @author Frédéric Moulé
 */
public class JavaFXAgent extends AbstractAgent {
    private static JavaFXAgent instance;

    /// // Constructeur(s)

    /**
     * Constructeur par défaut.
     */
    private JavaFXAgent() {
        this.addServiceDescription(createServiceDescription("javafx-agent", "gui"));
    }

    /// // Méthode(s) statique(s)

    /**
     * Charge le fichier FXML et renvoie l'instance Parent et le contrôleur
     * correspondants.
     *
     * @param fxmlFile : Fichier FXML
     * @return Couple composé du fichier FXML et de Parent
     * @throws IOException : En cas de problème I/O
     */
    public static Pair<Parent, Object> loadFXML(final File fxmlFile) throws IOException {
        if (!fxmlFile.exists()) {
            throw new IOException("Le fichier XML à charger n'existe pas : %s".formatted(fxmlFile.getAbsolutePath()));
        }
        final FXMLLoader fxmlLoader = new FXMLLoader(fxmlFile.toURI().toURL());
        fxmlLoader.setControllerFactory(new JavaFXControllerFactory());
        return Pair.of(fxmlLoader.load(), fxmlLoader.getController());
    }

    /**
     * Perment de récupérer l'instance du singleton.
     *
     * @return instance du singleton
     */
    public static JavaFXAgent getJavaFXAgentInstance() {
        if (JavaFXAgent.instance == null) {
            instance = new JavaFXAgent();
        }
        return JavaFXAgent.instance;
    }

    /// // Démarrage et arrêt de l'agent

    @Override
    protected void doSetUp() throws Exception {
        super.doSetUp();
        try {
            Application.launch(JavaFXApplication.class);
        } catch (Exception exception) {
            final Throwable cause = exception.getCause();
            if (cause instanceof InterruptedException interruptedException) {
                this.getLogger().info("JavaFX plateforme interrompue ---> c'est normal");
            } else {
                this.getLogger().error(exception.getMessage(), exception);
                throw new RuntimeException(exception);
            }
        }
    }

    @Override
    protected void doTakeDown() throws Exception {
        Platform.runLater(Platform::exit);
        super.doTakeDown();
    }

    /// // Classe(s) interne(s)

    /**
     * <p>
     * Factory de base utilisée afin de construire les différents controlleurs
     * </p>
     */
    private static class JavaFXControllerFactory implements Callback<Class<?>, Object> {

        @Override
        public Object call(final Class<?> aClass) {
            try {
                final Constructor<?> constructor = aClass.getConstructor();
                final Object newInstance = constructor.newInstance();
                if (newInstance instanceof AbstractJavaFXController abstractJavaFXController) {
                    abstractJavaFXController.setJavaFXAgent(JavaFXAgent.instance);
                }
                return newInstance;
            } catch (final Exception exception) {
                throw new RuntimeException(exception);
            }
        }
    }


}
