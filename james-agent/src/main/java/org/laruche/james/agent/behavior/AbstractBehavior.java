package org.laruche.james.agent.behavior;

import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.domain.DFService;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAException;
import org.laruche.james.agent.JadeMessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * <p>
 * Classe de base pour la définition des comportements des agents. <br />
 * </p>
 *
 * @author Frédéric Moulé
 * @see SimpleBehaviour
 */
public abstract class AbstractBehavior extends SimpleBehaviour
        implements JadeMessageHandler {
    private static final DFAgentDescription EMPTY_DF_AGENT_DESCRIPTION = new DFAgentDescription();
    private final transient ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private transient boolean isDone;
    private Logger logger;

    /// // Construceurs

    public AbstractBehavior() {
        this.isDone = false;
        this.logger = LoggerFactory.getLogger(AbstractBehavior.class);
    }

    /**
     * Méthode permettant de mettre fin à l'exécution du comportement. <br />
     */
    protected void finish() {
        lock.writeLock().lock();
        try {
            this.isDone = true;
        } finally {
            lock.writeLock().unlock();
        }
    }

    @Override
    public boolean done() {
        lock.readLock().lock();
        try {
            return isDone;
        } finally {
            lock.readLock().unlock();
        }
    }

    /// // Méthode(s) générale(s)

    /**
     * Retourne la liste des descriptions des agents correspondant à l'instance de la
     * classe <b>DFAgentDescription</b>.
     *
     * @param dfAgentDescription : critère de recherche
     * @return liste des descriptions des agents
     * @throws FIPAException : En cas de problèmes
     * @see DFAgentDescription
     */
    protected List<DFAgentDescription> searchAgentDescriptions(final DFAgentDescription dfAgentDescription) throws FIPAException {
        final Agent currentAgent = this.getAgent();
        if (currentAgent == null) {
            return new ArrayList<>();
        }
        return List.of(DFService.search(currentAgent, (dfAgentDescription == null ? EMPTY_DF_AGENT_DESCRIPTION : dfAgentDescription)));
    }

    /**
     * Méthode permettant de retourner toutes les descriptions
     * des agents de la plateforme.
     *
     * @return liste des descriptions des agents.
     * @throws FIPAException : En cas de problèmes lors de la recherche
     */
    protected List<DFAgentDescription> searchAllAgentDescriptions() throws FIPAException {
        return searchAgentDescriptions(EMPTY_DF_AGENT_DESCRIPTION);
    }

    /// // Getters & Setters

    public Logger getLogger() {
        return logger;
    }

    public void setLogger(final Logger logger) {
        this.logger = logger;
    }
}