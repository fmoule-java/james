package org.laruche.james.agent.javafx;

import org.laruche.james.plugin.HasPluginMananger;
import org.laruche.james.plugin.PluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * Classe abstraite utilisée pour construire les controlleurs
 * utilités avec les agents JavaFXAgent. <br />
 * </p>
 *
 * @see JavaFXAgent
 */
public abstract class AbstractJavaFXController implements HasPluginMananger {
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractJavaFXController.class);
    protected JavaFXAgent javaFXAgent;

    /// // Constructeurs :

    public AbstractJavaFXController() {
        javaFXAgent = null;
    }

    public void initialize() {
        LOGGER.info("Initialisation du controlleur");
    }

    /// // Getters & Setters :

    public void setJavaFXAgent(final JavaFXAgent javaFXAgent) {
        this.javaFXAgent = javaFXAgent;
    }

    public JavaFXAgent getJavaFXAgent() {
        return javaFXAgent;
    }

    public PluginManager getPluginManager() {
        if (this.javaFXAgent == null) {
            return null;
        }
        return this.javaFXAgent.getPluginManager();
    }

    @Override
    public void setPluginManager(final PluginManager pluginManager) {
        throw new RuntimeException("Ne doit pas être utilisé depuis cette classe");
    }
}
