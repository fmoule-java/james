package org.laruche.james.agent;

import jade.domain.FIPAAgentManagement.ServiceDescription;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * <p>
 * Classe utilitaire regroupant toutes les fonctions utilitaires concernant
 * les agents et leurs paramètres. <br />
 * </p>
 *
 * @see ServiceDescription
 */
public class AgentUtils {

    private AgentUtils() {
        // EMPTY
    }

    /// // éthode(s) utilitaire(s) :

    /**
     * Méthode statique permettant de créer une description de service
     * pour JADE ou James. <br />
     *
     * @param serviceName :  nom du service
     * @param type        : type du service
     * @return instance de la description du service
     * @see ServiceDescription
     */
    public static ServiceDescription createServiceDescription(final String serviceName, final String type) {
        final ServiceDescription serviceDescription = new ServiceDescription();
        if (!isEmpty(serviceName)) {
            serviceDescription.setName(serviceName);
        }
        if (!isEmpty(type)) {
            serviceDescription.setType(type);
        }
        return serviceDescription;
    }

    public static ServiceDescription createServiceDescription(final String serviceName) {
        return createServiceDescription(serviceName, null);
    }
}
