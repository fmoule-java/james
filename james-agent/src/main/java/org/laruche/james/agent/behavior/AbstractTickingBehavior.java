package org.laruche.james.agent.behavior;

import java.time.Duration;
import java.time.LocalDateTime;

import static java.time.Duration.between;
import static java.time.Duration.ofMillis;
import static java.time.LocalDateTime.now;

/**
 * <p>
 * Classe abstraite de base utilisée pour définir tous les comportements
 * cycliques et devant s'exécuter à un intervalle de temps donné. <br />
 * </p>
 *
 * @see AbstractBehavior
 * @see org.laruche.james.agent.AbstractAgent
 */
public abstract class AbstractTickingBehavior extends AbstractBehavior {
    private final Duration duration;
    private transient LocalDateTime lastTimestamp;

    /// // Constructeur(s)

    /**
     * Constructeur avec le délai. <br />
     *
     * @param duration : durée sous la forme d'une instance de la classe Duration
     * @see Duration
     */
    public AbstractTickingBehavior(final Duration duration) {
        this.duration = duration;
        this.lastTimestamp = now();
    }

    /**
     * Constructeur du comportement
     *
     * @param delayInMillis : délai en millisecondes
     */
    public AbstractTickingBehavior(final long delayInMillis) {
        this(ofMillis(delayInMillis));
    }

    /// // Méthode(s) de la classe Behavior

    @Override
    public void action() {
        if (between(lastTimestamp, now()).compareTo(duration) > 0) {
            this.doClickAction();
            lastTimestamp = now();
        }
    }

    /**
     * Exécute l'action.
     */
    protected abstract void doClickAction();

}
