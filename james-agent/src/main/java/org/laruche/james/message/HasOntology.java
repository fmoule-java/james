package org.laruche.james.message;

import jade.content.onto.Ontology;

import java.util.Collection;
import java.util.Set;

/**
 * <p>
 * Interface représentant tous les objets possédant
 * une ontologie. <br />
 * </p>
 *
 * @author Frédéric Moulé
 * @see jade.content.onto.Ontology
 */
public interface HasOntology {

    /**
     * Récupère l'ontologie
     *
     * @return ontologie
     */
    Set<Ontology> getOntologies();

    /**
     * Initialise l'ontologie.
     *
     * @param ontology : ontologie
     */
    void addOntology(final Ontology ontology);

    /// // Méthode(s) par défaut

    /**
     * Méthode permettant d'ejouter une coillection d'ontologie.
     *
     * @param ontologies : collection d'ontologie
     */
    default void addOntologies(final Collection<Ontology> ontologies) {
        if (ontologies == null || ontologies.isEmpty()) {
            return;
        }
        for (Ontology ontology : ontologies) {
            this.addOntology(ontology);
        }
    }

}
