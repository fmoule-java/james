package org.laruche.james.message;

import jade.lang.acl.ACLMessage;

/**
 * <p>
 * Enumération permettant de lister les différentes
 * définitions des performatives utilisables par les messages ACL. <br />
 * </p>
 *
 * @author Frédéric Moulé
 * @see ACLMessage
 */
public enum MessagePerformative {
    ACCEPT_PROPOSAL(ACLMessage.ACCEPT_PROPOSAL),
    AGREE(ACLMessage.AGREE),
    CANCEL(ACLMessage.CANCEL),
    CFP(ACLMessage.CFP),
    CONFIRM(ACLMessage.CONFIRM),
    DISCONFIRM(ACLMessage.DISCONFIRM),
    FAILURE(ACLMessage.FAILURE),
    INFORM(ACLMessage.INFORM),
    INFORM_IF(ACLMessage.INFORM_IF),
    NOT_UNDERSTOOD(ACLMessage.NOT_UNDERSTOOD),
    PROPOSE(ACLMessage.PROPOSE),
    QUERY_IF(ACLMessage.QUERY_IF),
    QUERY_REF(ACLMessage.QUERY_REF),
    REFUSE(ACLMessage.REFUSE),
    REJECT_PROPOSAL(ACLMessage.REJECT_PROPOSAL),
    REQUEST(ACLMessage.REQUEST),
    REQUEST_WHEN(ACLMessage.REQUEST_WHEN),
    REQUEST_WHENEVER(ACLMessage.REQUEST_WHENEVER),
    SUBSCRIBE(ACLMessage.SUBSCRIBE),
    PROXY(ACLMessage.PROXY),
    PROPAGATE(ACLMessage.PROPAGATE),
    UNKNOWN(ACLMessage.UNKNOWN);

    private final int performative;

    /// Définition des constructeur(s) :

    /**
     * Constructeur de l'énumération
     *
     * @param performative : code sous forme
     */
    MessagePerformative(int performative) {
        this.performative = performative;
    }

    /// Méthodes générales :

    /**
     * Méthode permenttant de retournant le code performatif
     * du message ACL. <br />
     *
     * @return code
     */
    public int toACLPerformative() {
        return performative;
    }

    /// Méthodes statiques :

    /**
     * <p>
     * Méthode statique permettant de retourner l'instance de l'énumaration
     * correspond au code passé en paramètre. <br />
     * </p>
     *
     * @param code : code ACL
     * @return instance de l'énumération
     */
    public static MessagePerformative fromCode(int code) {
        for (MessagePerformative messagePerformative : MessagePerformative.values()) {
            if (messagePerformative.toACLPerformative() != code) {
                continue;
            }
            return messagePerformative;
        }
        return UNKNOWN;
    }
}
