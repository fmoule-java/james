package org.laruche.james.plugin;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * Plugin permettant la gestion d'agents
 * </p>
 *
 * @author Frédéric Moulé
 */
public class MainAgentPlugin extends AbstractAgentPlugin {

    /// // Constructeur(s) :

    /**
     * Constructeur du plugin avec les différents paramètres
     * définissant la plateforme. <br />
     *
     * @param platformId          :  identifiant de la plateforme
     * @param conteinerProperties : dictionnaire des différentes propriétés.
     */
    private MainAgentPlugin(final String platformId, final Map<String, Object> conteinerProperties) {
        super(platformId, true, conteinerProperties);
    }

    /// // Méthode(s) générale(s)



    /// // Méthode(s) statique(s)

    public static MainAgentPlugin createMainAgentPlugin(final String platformId, final Map<String, Object> conteinerProperties) {
        return new MainAgentPlugin(platformId, conteinerProperties);
    }

    public static MainAgentPlugin createMainAgentPlugin(final String platformId) {
        return createMainAgentPlugin(platformId, new HashMap<>());
    }

}