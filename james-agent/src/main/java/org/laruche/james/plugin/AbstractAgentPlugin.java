package org.laruche.james.plugin;

import jade.core.Agent;
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.wrapper.ControllerException;
import jade.wrapper.StaleProxyException;
import org.apache.commons.collections4.MapUtils;
import org.laruche.james.agent.AbstractAgent;
import org.laruche.james.agent.state.AgentState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.util.*;

import static jade.core.Profile.MAIN;
import static jade.core.Profile.PLATFORM_ID;
import static java.lang.Thread.sleep;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.laruche.james.agent.state.AgentState.*;

/**
 * Classe représentant toutes les instances de type AgentPlugin. <br />
 *
 * @author Frédéric Moulé
 */
public abstract class AbstractAgentPlugin extends AbstractPlugin {
    public static final int DEFAULT_CONTAINER_PORT = 1099;
    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractAgentPlugin.class);
    protected static final Object[] EMPTY_ARGS = {};
    protected final Map<String, Agent> agentsToStart = new HashMap<>();
    protected final boolean isMainContainer;
    protected final Map<String, Object> conteinerProperties;
    protected transient AgentContainer mainContainer;

    /// // Constructeur(s)

    /**
     * Constructeur de base des instances de l'interface AgentPlugin. <br />
     *
     * @param id                  : id du plugin
     * @param isMainContainer     : booléen montrant si le conteneur est principal ou pas
     * @param conteinerProperties : toutes les propriétés du plugin
     */
    public AbstractAgentPlugin(final String id, final boolean isMainContainer, final Map<String, Object> conteinerProperties) {
        super(id);
        this.isMainContainer = isMainContainer;
        this.conteinerProperties = conteinerProperties;
    }

    /// // Implémentation de l'interface Plugin :

    @Override
    protected void doStop() throws Exception {
        try {
            if (mainContainer != null) {
                this.mainContainer.kill();
                sleep(1000);
            }
        } catch (final StaleProxyException staleProwyException) {
            LOGGER.info("Container is, maybe, already closed");
        } catch (final Exception controllerException) {
            LOGGER.error(controllerException.getMessage(), controllerException);
            throw new Exception(controllerException.getMessage(), controllerException);
        }
    }

    @Override
    protected void doStart() throws Exception {
        mainContainer = this.createJADEContainer();
        try {
            mainContainer.start();
            final Set<String> agentNames = agentsToStart.keySet();
            for (String agentName : agentNames) {
                mainContainer.acceptNewAgent(agentName, agentsToStart.get(agentName)).start();
            }
            sleep(500L);
            for (String agentName : agentNames) {
                if (this.getAgentState(agentName) != DELETED) {
                    continue;
                }
                throw new Exception("Un des agents n'a pas pu démarré dû à des problèmes d'initialisation");
            }
        } catch (final Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
            this.stop();
            throw exception;
        }
    }

    /**
     * Méthode permettant de créer le conteneur JADE
     * englobé par l'instance courante.
     *
     * @return conteneur JADE
     * @see AgentContainer
     */
    protected AgentContainer createJADEContainer() {
        final Profile profile = new ProfileImpl();
        this.conteinerProperties.put(MAIN, isMainContainer);
        this.conteinerProperties.put(PLATFORM_ID, this.getId());
        for (String propName : conteinerProperties.keySet()) {
            profile.setParameter(propName, String.valueOf(this.conteinerProperties.get(propName)));
        }
        return Runtime.instance().createMainContainer(profile);
    }

    /// // Méthodes de gestion des agents :

    public void addAgentToStart(final String name, final Class<? extends Agent> agentClazz, final Object[] args) {
        try {
            final List<Object> theArgs = List.of(args);
            final Constructor<? extends Agent> constructor;
            final Agent createdAgent;
            if (theArgs.isEmpty()) {
                constructor = agentClazz.getConstructor();
                createdAgent = constructor.newInstance();
            } else {
                final List<? extends Class<?>> classes = theArgs.stream().map(Object::getClass).toList();
                constructor = agentClazz.getConstructor(classes.toArray(new Class[0]));
                createdAgent = constructor.newInstance(args);
            }
            if (createdAgent instanceof AbstractAgent abstractAgent) {
                abstractAgent.setPluginManager(this.getPluginManager());
            }
            this.addAgentToStart(name, createdAgent);
        } catch (final Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
            throw new RuntimeException(exception.getMessage(), exception);
        }
    }

    public void addAgentToStart(final String name, final Class<? extends Agent> agentClazz) {
        addAgentToStart(name, agentClazz, EMPTY_ARGS);
    }

    /**
     * Ajoute une instance d'agent dans le plugin;
     *
     * @param name  : nom de l'agent
     * @param agent : instance de l'agent
     */
    public void addAgentToStart(final String name, final Agent agent) {
        try {
            if (mainContainer != null) {
                this.mainContainer
                        .acceptNewAgent(name, agent)
                        .start();
            } else {
                this.agentsToStart.put(name, agent);
            }
            if (agent instanceof AbstractAgent) {
                ((AbstractAgent) agent).setPluginManager(this.getPluginManager());
            }
        } catch (final Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
            throw new RuntimeException(exception.getMessage(), exception);
        }
    }

    public void clearAgents() {
        this.agentsToStart.clear();
    }

    /**
     * Méthode permettant de tester si les propriétés
     * du conteneur sous-jacent possède la propriété dont
     * le nom est passé en paramètre. <br />
     *
     * @param propertyName : nom de la propriété
     * @return booléen reésentant le résultat
     */
    public boolean containsContainerProperty(final String propertyName) {
        if (isEmpty(propertyName)) {
            return false;
        }
        return this.conteinerProperties.containsKey(propertyName);
    }


    /// // Getters & Setters

    /**
     * <p>
     * Méthode permettant de retourner les agents
     * gérés par le plugin. <br />
     * Par contre, cette méthode ne renvoie pas les agents qui appartiendraient
     * à des conteneurs secondaires ou "esclave"
     * </p>
     *
     * @return agents
     * @see Agent
     */
    public Set<Agent> getInternalAgents() {
        return new HashSet<>(this.agentsToStart.values());
    }

    /**
     * Retourne l'état de l'agent identifié par le nom passé ne paramètre. <br />
     *
     * @param agentName : nom de l'agent
     * @return état de l'agent
     * @see AgentState
     */
    public AgentState getAgentState(final String agentName) {
        if (MapUtils.isEmpty(this.agentsToStart)) {
            return UNKNWON;
        }
        final Agent agent = this.agentsToStart.get(agentName);
        if (agent == null) {
            return UNKNWON;
        }
        return fromJadeAgentState(agent.getAgentState());
    }

    @Override
    public void setPluginManager(final PluginManager pluginManager) {
        super.setPluginManager(pluginManager);
        for (Agent agent : agentsToStart.values()) {
            if (!(agent instanceof AbstractAgent abstractAgent)) {
                continue;
            }
            abstractAgent.setPluginManager(pluginManager);
        }
    }

    public AgentController getAgentController(final String agentLocalName) {
        return getAgentController(agentLocalName, false);
    }

    public AgentController getAgentController(final String agentLocalName, boolean isGuid) {
        try {
            return this.mainContainer.getAgent(agentLocalName, isGuid);
        } catch (final ControllerException controllerException) {
            return null;
        }
    }

    public <T> T getContainerProperty(final String propName) {
        if (!this.containsContainerProperty(propName)) {
            return null;
        }
        //noinspection unchecked
        return (T) this.conteinerProperties.get(propName);
    }
}
