package org.laruche.james.plugin;

import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentContainer;

import java.util.HashMap;
import java.util.Map;

import static jade.core.Profile.*;

/**
 * <p>
 * Classe représentant les instances de l'interface AgentPlugin
 * qui sont secondaires. <br />
 * </p>
 *
 * @author Frédéric Moulé
 * @see AbstractAgentPlugin
 */
public class SlaveAgentPlugin extends AbstractAgentPlugin {

    /// // Constructeur(s)

    public SlaveAgentPlugin(final String platformId, final Map<String, Object> conteinerProperties) {
        super(platformId, false, conteinerProperties);
    }

    /// // Méthode(s) de la classe AbstractAgentPlugin

    @Override
    protected AgentContainer createJADEContainer() {
        final Profile profile = new ProfileImpl();
        this.conteinerProperties.put(MAIN, isMainContainer);
        this.conteinerProperties.put(PLATFORM_ID, this.getId());
        for (String propName : conteinerProperties.keySet()) {
            profile.setParameter(propName, String.valueOf(this.conteinerProperties.get(propName)));
        }
        return Runtime.instance().createAgentContainer(profile);
    }

    /// // Méthode(s) statique(s) :

    public static SlaveAgentPlugin createSlaveAgentPlugin(final String platformId, final Map<String, Object> containerProperties) {
        /*if (!containerProperties.containsKey(MAIN_HOST)) {
            throw new IllegalArgumentException("Il manque la propriété nommée MAIN_HOST de la classe java.core.Profile");
        }
        if (!containerProperties.containsKey(MAIN_PORT)) {
            throw new IllegalArgumentException("Il manque la propriété nommée MAIN_PORT de la classe java.core.Profile");
        }*/
        return new SlaveAgentPlugin(platformId, containerProperties);
    }

    public static SlaveAgentPlugin createSlaveAgentPlugin(final String platformId,
                                                          final String mainContainerHost,
                                                          final int mainContainerPort) {
        final Map<String, Object> conteinerProperties = new HashMap<>();
        conteinerProperties.put(MAIN_HOST, mainContainerHost);
        conteinerProperties.put(MAIN_PORT, mainContainerPort);
        return createSlaveAgentPlugin(platformId, conteinerProperties);
    }
}
