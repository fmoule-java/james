package org.laruche.james.concurrent;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.laruche.james.test.AbstractAgentTestCase;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Future;

import static java.lang.Thread.sleep;
import static org.assertj.core.api.Assertions.assertThat;

class AgentExecutorServiceTest extends AbstractAgentTestCase<String> {
    private final static Logger LOGGER = LoggerFactory.getLogger(AgentExecutorServiceTest.class);
    private AgentExecutorService executorService;

    /// // Initialisation :

    @BeforeEach
    public void setUp() {
        this.executorService = new AgentExecutorService(this.mainAgentPlugin);
    }

    @Override
    @AfterEach
    public void tearDown() throws Exception {
        super.tearDown();
    }

    ///// Unit tests :

    @Test
    public void shouldDivideLongValues() {
        assertThat(300L / 100L).isEqualTo(3L);
        assertThat(320L / 100L).isEqualTo(3L);
    }

    @Test
    public void shouldExecuteARunnable() throws Exception {
        final StringBuilder buffer = new StringBuilder();
        final Future<?> future = this.executorService.submit(() -> {
            for (int i = 0; i < 5; i++) {
                buffer.append(i);
            }
        });
        this.startAgentPlugin();
        sleep(500);
        assertThat(future.isDone()).isTrue();
        assertThat(buffer.toString()).isEqualTo("01234");
    }

    @Test
    public void shouldExecuteACallable() throws Exception {
        this.startAgentPlugin();
        final Future<String> future = this.executorService.submit(() -> {
            final StringBuilder buffer = new StringBuilder();
            for (int i = 0; i < 7; i++) {
                buffer.append(i);
            }
            return buffer.toString();
        });
        sleep(700);
        assertThat(future.isDone()).isTrue();
        assertThat(future.get()).isEqualTo("0123456");
    }

    @Test
    public void shouldShutdown() throws Exception {
        final StringBuffer buffer = new StringBuffer("");
        executorService.submit(() -> {
            try {
                sleep(10000);
                buffer.append("task1");
            } catch (final InterruptedException e) {
                LOGGER.warn("Tâche 'tack1' interrompue => ce qui est normal !!!");
            }
        });
        executorService.submit(() -> {
            try {
                sleep(12000);
                buffer.append("task2");
            } catch (final InterruptedException e) {
                LOGGER.warn("Tâche 'tack2' interrompue => ce qui est normal !!!");
            }
        });
        this.startAgentPlugin();
        this.executorService.shutdown();
        sleep(800);
        assertThat(executorService.isShutdown()).isTrue();
        sleep(800);
        assertThat(buffer.toString()).isEmpty();
    }



}