package org.laruche.james.agent;

import jade.content.onto.BeanOntology;
import jade.content.onto.Ontology;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.glassfish.jersey.internal.guava.Lists.newArrayList;
import static org.laruche.james.message.MessageUtils.DEFAULT_LANGUAGE;

class AbstractAgentTest {

    /// // Tests unitaires :

    @Test
    void shouldGetCodecName() {
        assertThat(DEFAULT_LANGUAGE.getName()).isNotNull();
    }

    @Test
    public void shouldGetDescriptions() {
        final TestAgent testAgent = new TestAgent();
        final DFAgentDescription agentDescription = testAgent.getAgentDescription();
        assertThat(agentDescription).isNotNull();
    }

    @Test
    public void shouldGetDescriptionWithParameters() {
        final TestAgent testAgent = new TestAgent();
        testAgent.addOntology(TestOntology.TEST_ONTOLOGY);
        final DFAgentDescription agentDescription = testAgent.getAgentDescription();
        assertThat(agentDescription).isNotNull();

        // Vérification des ontologies
        final List<?> ontologies = newArrayList(agentDescription.getAllOntologies());
        assertThat(ontologies).isNotNull();
        assertThat(ontologies.size()).isEqualTo(1);
        assertThat(ontologies.getFirst()).isEqualTo(TestOntology.TEST_ONTOLOGY.getName());

        // Vérification du langage utilisé
        final ArrayList<?> usedLanguages = newArrayList(agentDescription.getAllLanguages());
        assertThat(usedLanguages).isNotNull();
        assertThat(usedLanguages).isNotEmpty();
        assertThat(usedLanguages.size()).isEqualTo(1);
        assertThat(usedLanguages.getFirst()).isEqualTo(DEFAULT_LANGUAGE.getName());
    }

    @Test
    public void shouldAddOntology() {
        final TestAgent testAgent = new TestAgent();
        testAgent.addOntology(TestOntology.TEST_ONTOLOGY);
        final Set<Ontology> ontologies = testAgent.getOntologies();
        assertThat(ontologies).isNotEmpty();
        assertThat(ontologies).contains(TestOntology.TEST_ONTOLOGY);
    }

    /// // Classe(s) interne(s)

    private static class TestAgent extends AbstractAgent {

    }

    private static class TestOntology extends BeanOntology {
        public static final TestOntology TEST_ONTOLOGY = new TestOntology();

        /// // Constructeur(s):

        public TestOntology() {
            super("testOntology");
        }
    }
}