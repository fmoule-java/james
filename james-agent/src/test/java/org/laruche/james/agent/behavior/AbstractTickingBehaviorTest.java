package org.laruche.james.agent.behavior;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.laruche.james.agent.AbstractAgent;
import org.laruche.james.test.AbstractAgentTestCase;
import org.laruche.james.test.bean.TestResult;

import java.time.Duration;

import static java.time.Duration.ofMillis;
import static org.assertj.core.api.Assertions.assertThat;

public class AbstractTickingBehaviorTest extends AbstractAgentTestCase<Integer> {

    /// // Initialisation

    @BeforeEach
    public void setUp() {
        this.setTestResult(new TestResult<>(0));
    }

    @Override
    @AfterEach
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    /// // Tests unitaires :

    @Test
    void shouldExecuteTask() throws Exception {
        this.mainAgentPlugin.addAgentToStart("testAgent", new TickingTestAgent(ofMillis(500), this.getTestResult()));
        this.startAgentPlugin();
        assertThat(this.mainAgentPlugin.isStarted()).isTrue();
        assertThat(this.getTestResult().getValue()).isGreaterThanOrEqualTo(1);
    }

    @Test
    public void shouldExecuteMultipleTimes() throws Exception {
        this.mainAgentPlugin.addAgentToStart("testAgent", new TickingTestAgent(ofMillis(500), this.getTestResult()));
        this.startAgentPlugin();
        assertThat(this.mainAgentPlugin.isStarted()).isTrue();
        assertThat(this.getTestResult().getValue()).isGreaterThanOrEqualTo(3);
    }

    /// // Classe(s) Interne(s)

    private static class TickingTestAgent extends AbstractAgent {

        public TickingTestAgent(final Duration duration, final TestResult<Integer> testResult) {
            this.addBehaviour(new TestTicketingBehavior(duration, testResult));
        }

    }

    private static class TestTicketingBehavior extends AbstractTickingBehavior {
        private final TestResult<Integer> testResult;

        /// // Constructeur(s) :

        private TestTicketingBehavior(final Duration duration,
                                      final TestResult<Integer> testResult) {
            super(duration);
            this.testResult = testResult;
        }

        private TestTicketingBehavior(final long delay,
                                      final TestResult<Integer> testResult) {
            super(delay);
            this.testResult = testResult;
        }

        @Override
        protected void doClickAction() {
            testResult.setValue(this.testResult.getValue() + 1);
        }
    }
}