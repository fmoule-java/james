package org.laruche.james.agent;

import jade.content.AgentAction;
import jade.content.onto.BeanOntology;
import jade.content.onto.Ontology;
import jade.core.AID;
import org.laruche.james.agent.behavior.AbstractOneShotBehavior;
import org.laruche.james.message.HasOntology;
import org.laruche.james.message.MessagePerformative;
import org.laruche.james.test.agent.SimpleTestAgent;

import java.io.Serializable;
import java.util.Set;

import static jade.core.AID.ISLOCALNAME;

/**
 * <p>
 * Agent utlisé dans les tests permettant d'envoyer des messages.
 * </p>
 *
 * @author Frédéric Moulé
 */
public class TestSendingMessageAgent extends SimpleTestAgent {

    /// // Méthodes générales :

    public void addMessageToSend(final String receiverAgentName,
                                 final int performative,
                                 final String msgContent) {
        this.addBehaviour(new TestSendingMessageBehavior(receiverAgentName, performative, msgContent));
    }

    public void addMessageToSend(final String receiverAgentName,
                                 final int performative,
                                 final BeanOntology ontology,
                                 final AgentAction action) {
        final TestSendingMessageBehavior behaviour = new TestSendingMessageBehavior(receiverAgentName, performative, action);
        behaviour.addOntology(ontology);
        this.addBehaviour(behaviour);
    }

    /**
     * Permet d'envoyer un message sous forme d'objet Serializable
     *
     * @param receiverAgentName       : AID du destinatiare du message
     * @param performative   : code
     * @param ontology       : ontologie utilisé par le message
     * @param messageContent : contenu du message sous forme d'instance de l'interface Serializable
     * @see java.io.Serializable
     */
    public void addMessageToSend(final String receiverAgentName,
                                 final int performative,
                                 final BeanOntology ontology,
                                 final Serializable messageContent) {
        final TestSendingMessageBehavior behaviour = new TestSendingMessageBehavior(receiverAgentName, performative, messageContent);
        behaviour.addOntology(ontology);
        this.addBehaviour(behaviour);
    }

    /// // Classe(s) interne(s)

    public static class TestSendingMessageBehavior extends AbstractOneShotBehavior implements HasOntology {
        protected final String receiverAgentName;
        protected final boolean isGlobalName;
        protected final int performative;
        protected final Object content;
        protected Ontology ontology = null;

        /// // Constructeur(s)

        public TestSendingMessageBehavior(final String receiverAgentName, final Boolean isGlobalName, int performative, Object message) {
            this.receiverAgentName = receiverAgentName;
            this.isGlobalName = (isGlobalName != null && isGlobalName);
            this.performative = performative;
            this.content = message;
        }

        public TestSendingMessageBehavior(final String receiverAgentName, final int performative, final Object message) {
            this(receiverAgentName, ISLOCALNAME, performative, message);
        }

        public TestSendingMessageBehavior(final String receiverAgentName, final MessagePerformative performative, final Object message) {
            this(receiverAgentName, ISLOCALNAME, performative.toACLPerformative(), message);
        }

        /// // Méthode(s) de la classe Object :

        @Override
        public boolean equals(final Object obj) {
            if (!(obj instanceof TestSendingMessageBehavior)) {
                return false;
            }
            return super.equals(obj);
        }

        @Override
        protected void doOneShotAction() {
            final AID receiverId = new AID(receiverAgentName, isGlobalName);
            try {
                switch (content) {
                    case String stringMessage -> this.sendMessage(receiverId, performative, stringMessage);
                    case AgentAction agentAction ->
                            this.sendMessage(receiverId, performative, this.ontology, agentAction);
                    case Serializable serContentMessage ->
                            this.sendMessage(receiverId, performative, this.ontology, serContentMessage);
                    case null, default ->
                            throw new Exception("Le contenu n'est ni un String, ni une instance de AgentAction");
                }
            } catch (final Exception exception) {
                this.getLogger().error(exception.getMessage(), exception);
                this.sendFailureMessage(receiverId, "Erreur " + exception.getMessage());
            }
        }

        /// // Getters & Setters :

        public Set<Ontology> getOntologies() {
            return Set.of(ontology);
        }

        public void addOntology(final Ontology ontology) {
            this.ontology = ontology;
        }

    }

}
