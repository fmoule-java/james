package org.laruche.james.agent.message;

import org.junit.jupiter.api.Test;
import org.laruche.james.message.MessagePerformative;

import static org.assertj.core.api.Assertions.assertThat;
import static org.laruche.james.message.MessagePerformative.*;

class MessagePerformativeTest {

    /// Tests unitaires

    @Test
    public void shouldReturnEnumFromCode() {

        // On bouclde
        for (MessagePerformative messagePerformative : values()) {
            assertThat(fromCode(messagePerformative.toACLPerformative())).isEqualTo(messagePerformative);
        }

        // Pour les codes inconnus
        assertThat(fromCode(100000)).isEqualTo(UNKNOWN);
        assertThat(fromCode(-67)).isEqualTo(UNKNOWN);
    }

}