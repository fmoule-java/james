package org.laruche.james.agent.behavior;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.laruche.james.agent.TestSendingMessageAgent;
import org.laruche.james.bean.TestBeanOntology.AddTestBeanAction;
import org.laruche.james.test.AbstractAgentTestCase;
import org.laruche.james.test.bean.TestResult;

import static jade.lang.acl.ACLMessage.INFORM;
import static org.assertj.core.api.Assertions.assertThat;
import static org.laruche.james.bean.TestBeanOntology.TEST_BEAN_ONTOLOGY;

public class AbstractBehaviorTest extends AbstractAgentTestCase<String> {
    private final TestSendingMessageAgent senderAgent = new TestSendingMessageAgent();

    /// // Initialisation

    @BeforeEach
    public void setUp() {

        // Liste des agents à démarrer :
        this.mainAgentPlugin.addAgentToStart(TEST_MANAGER_AGENT_NAME, this.testManagerAgent);
        this.mainAgentPlugin.addAgentToStart("senderAgent", senderAgent);

        // Initialisation de l'ontologie :
        this.testManagerAgent.addOntology(TEST_BEAN_ONTOLOGY);
    }

    @AfterEach
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    ///// Unit tests :

    @Test
    public void shouldSendMessage() throws Exception {
        senderAgent.addMessageToSend("testManagerAgent", INFORM, "Message envoyé");
        startAgentPlugin();
        final TestResult<String> testResult = this.getTestResult();
        assertThat(testResult.isInError()).isFalse();
        assertThat(testResult).isNotNull();
        assertThat(testResult.getValue()).isEqualTo("Message envoyé");
    }


    @Test
    public void shouldSendMessageWithAction() throws Exception {
        senderAgent.addMessageToSend("testManagerAgent", INFORM, TEST_BEAN_ONTOLOGY, new AddTestBeanAction("frederic", "moule"));
        startAgentPlugin();
        final TestResult<String> testResult = this.getTestResult();
        assertThat(testResult).isNotNull();
        assertThat(testResult.isInError()).isFalse();
        assertThat(testResult.getValue().contains("createTestBean")).isTrue();
    }

}