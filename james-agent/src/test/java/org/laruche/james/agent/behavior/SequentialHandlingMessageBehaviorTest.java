package org.laruche.james.agent.behavior;

import jade.lang.acl.ACLMessage;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.laruche.james.test.AbstractAgentTestCase;
import org.laruche.james.test.agent.SimpleTestAgent;
import org.laruche.james.test.bean.TestResult;

import static jade.lang.acl.MessageTemplate.MatchPerformative;
import static java.lang.Thread.sleep;
import static org.assertj.core.api.Assertions.assertThat;
import static org.laruche.james.message.MessagePerformative.REQUEST;
import static org.laruche.james.message.MessageUtils.createSimpleMessage;

public class SequentialHandlingMessageBehaviorTest extends AbstractAgentTestCase<String> {
    private final SimpleTestAgent testingAgent = new SimpleTestAgent();

    /// // Initialisation et fermeture :

    @BeforeEach
    public void setUp() {
        this.mainAgentPlugin.addAgentToStart("managerAgent", this.testManagerAgent);
        this.mainAgentPlugin.addAgentToStart("testingAgent", testingAgent);
    }

    @Override
    @AfterEach
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    ///// Tests unitaires :

    @Test
    void shouldExecuteAllTasks() throws Exception {
        final SequentialHandlingMessageBehavior seqBehaviour = new SequentialHandlingMessageBehavior();
        final TestResult<String> testResult = this.getTestResult();
        seqBehaviour.addSubBehaviour(new FirstTestBehavior(testResult));
        seqBehaviour.addSubBehaviour(new SecundTestBehavior(testResult));
        this.testingAgent.addBehaviour(seqBehaviour);
        this.startAgentPlugin();
        this.sendMessage(createSimpleMessage(testingAgent.getAID(), REQUEST, "message"));
        sleep(500);
        assertThat(testResult.getValue()).isEqualTo(";FirstBehaviour => message;SecundBehaviour => message");
    }


    ///// Classes internes :

    private static class FirstTestBehavior extends AbstractHandlingMessageBehavior {
        private final TestResult<String> testResult;

        FirstTestBehavior(final TestResult<String> testResult) {
            super(MatchPerformative(ACLMessage.REQUEST));
            this.testResult = testResult;
        }

        @Override
        public void doAction(final ACLMessage message) {
            final String currentValue = testResult.getValue();
            if (currentValue == null) {
                testResult.setValue(";FirstBehaviour => " + message.getContent());
            } else {
                testResult.setValue(currentValue + ";FirstBehaviour => " + message.getContent());
            }
        }
    }

    private static class SecundTestBehavior extends AbstractHandlingMessageBehavior {
        private final TestResult<String> testResult;

        SecundTestBehavior(final TestResult<String> testResult) {
            super(MatchPerformative(ACLMessage.REQUEST));
            this.testResult = testResult;
        }

        @Override
        public void doAction(final ACLMessage message) {
            final String currentValue = testResult.getValue();
            if (currentValue == null) {
                testResult.setValue(";SecundBehaviour => " + message.getContent());
            } else {
                testResult.setValue(currentValue + ";SecundBehaviour => " + message.getContent());
            }
        }
    }


}