package org.laruche.james.agent.javafx;

import javafx.application.Platform;
import javafx.scene.Parent;
import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.laruche.james.plugin.MainAgentPlugin;
import org.laruche.james.plugin.manager.DefaultPluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import static java.lang.Thread.sleep;
import static java.util.Objects.requireNonNull;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.laruche.james.agent.javafx.JavaFXAgent.getJavaFXAgentInstance;
import static org.laruche.james.plugin.MainAgentPlugin.createMainAgentPlugin;

@SuppressWarnings("resource")
public class JavaFXAgentTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(JavaFXAgentTest.class);
    private static final String BASE_PATH = requireNonNull(JavaFXAgentTest.class.getResource("/")).getPath();

    /// // Tests unitaires

    @Test
    public void shouldCreateInstance() {
        final JavaFXAgent javaFaxAgent = getJavaFXAgentInstance();
        assertThat(javaFaxAgent).isNotNull();
    }

    @Disabled("A lancer à la main")
    @Test
    public void shouldLoad() throws Exception {
        final JavaFXAgent javaFXAgentInstance = getJavaFXAgentInstance();
        assertThat(javaFXAgentInstance).isNotNull();
        final File testFxmlFile = new File(BASE_PATH + "/gui", "mainGUI.fxml");
        assertThat(testFxmlFile).isNotNull();
        assertThat(testFxmlFile).exists();
        final JavaFXToken<Pair<Parent, Object>> parentJavaFXToken = new JavaFXToken<>();
        Platform.startup(() -> {
            try {
                parentJavaFXToken.setToken(JavaFXAgent.loadFXML(testFxmlFile));
            } catch (final Exception exception) {
                parentJavaFXToken.setToken(null);
                LOGGER.error(exception.getMessage(), exception);
                fail(exception.getMessage());
            } finally {
                parentJavaFXToken.setExecuted(true);
            }
        });
        while (!parentJavaFXToken.isExecuted()) {
            LOGGER.debug("---> En attente du thread JavaFX");
            sleep(100);
        }
        final Parent parent = parentJavaFXToken.getToken().getLeft();
        assertThat(parent).isNotNull();
        final Object controller = parentJavaFXToken.getToken().getRight();
        assertThat(controller).isNotNull();
        if (!(controller instanceof AbstractJavaFXController abstractJavaFXController)) {
            fail("Le contrôleur n'est pas une instance de AbstractJavaFXController");
        } else {
            assertThat(abstractJavaFXController.getJavaFXAgent()).isNotNull();
            assertThat(abstractJavaFXController.getJavaFXAgent()).isEqualTo(getJavaFXAgentInstance());
        }
    }

    @Test
    public void shouldGetPluginManager() {
        final DefaultPluginManager pluginManager = new DefaultPluginManager();
        final MainAgentPlugin mainAgentPlugin = createMainAgentPlugin("testAgentPlugin");
        mainAgentPlugin.addAgentToStart("javaFXAgent", getJavaFXAgentInstance());
        pluginManager.addPlugin(mainAgentPlugin);

        // Test
        assertThat(mainAgentPlugin.getPluginManager()).isNotNull();
        final JavaFXAgent javaFXAgentInstance = getJavaFXAgentInstance();
        assertThat(javaFXAgentInstance).isNotNull();
        assertThat(javaFXAgentInstance.getPluginManager()).isNotNull();
    }

    /// // Classe(s) interne(s)

    private static class JavaFXToken<T> {
        private final transient ReentrantReadWriteLock lock;
        private T token;
        private boolean isExecuted;

        public JavaFXToken() {
            this.token = null;
            this.isExecuted = false;
            this.lock = new ReentrantReadWriteLock();
        }

        public boolean isExecuted() {
            this.lock.readLock().lock();
            try {
                return isExecuted;
            } finally {
                this.lock.readLock().unlock();
            }
        }

        public void setExecuted(boolean executed) {
            this.lock.writeLock().lock();
            try {
                isExecuted = executed;
            } finally {
                this.lock.writeLock().unlock();
            }
        }

        public T getToken() {
            this.lock.readLock().lock();
            try {
                return token;
            } finally {
                this.lock.readLock().unlock();
            }
        }

        public void setToken(T token) {
            this.lock.writeLock().lock();
            try {
                this.token = token;
            } finally {
                this.lock.writeLock().unlock();
            }
        }
    }

}