package org.laruche.james.agent.javafx;

import javafx.fxml.FXML;
import javafx.scene.control.Button;

/// // Classe(s) interne(s)

public class TestJavaFXController extends AbstractJavaFXController {

    @FXML
    private Button closeButton;

    /// // Evènements

    public void onClose() {
        LOGGER.info("---> Appel de onClose");
    }

    public Button getCloseButton() {
        return closeButton;
    }
}
