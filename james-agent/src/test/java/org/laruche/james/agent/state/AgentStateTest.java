package org.laruche.james.agent.state;

import jade.core.Agent;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.laruche.james.agent.state.AgentState.*;

class AgentStateTest {

    /// // Tests unitaires

    @Test
    public void shouldCreateInstance() {
        assertThat(fromJadeAgentStateCode(Agent.AP_IDLE)).isEqualTo(IDLE);
        assertThat(fromJadeAgentStateCode(Agent.AP_WAITING)).isEqualTo(WAITING);
        assertThat(fromJadeAgentStateCode(Agent.AP_SUSPENDED)).isEqualTo(SUSPENDED);
        assertThat(fromJadeAgentStateCode(Agent.AP_ACTIVE)).isEqualTo(ACTIVE);
        assertThat(fromJadeAgentStateCode(Agent.AP_DELETED)).isEqualTo(DELETED);
        assertThat(fromJadeAgentStateCode(null)).isEqualTo(UNKNWON);
    }

}