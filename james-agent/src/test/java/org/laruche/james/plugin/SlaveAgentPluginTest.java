package org.laruche.james.plugin;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import static jade.core.Profile.MAIN;
import static org.assertj.core.api.Assertions.assertThat;
import static org.laruche.james.plugin.AbstractAgentPlugin.DEFAULT_CONTAINER_PORT;
import static org.laruche.james.plugin.SlaveAgentPlugin.createSlaveAgentPlugin;

class SlaveAgentPluginTest {
    private MainAgentPlugin masterAgentPlugin;
    private SlaveAgentPlugin slaveAgentPlugin;

    /// // Initialisation

    @AfterEach
    void tearDown() throws Exception {
        if (slaveAgentPlugin != null && slaveAgentPlugin.isStarted()) {
            slaveAgentPlugin.stop();
        }
        if (masterAgentPlugin != null && masterAgentPlugin.isStarted()) {
            masterAgentPlugin.stop();
        }
    }

    /// // Tests unitaires

    @Test
    public void shouldHaveSpecificProperties() throws Exception {
        masterAgentPlugin = MainAgentPlugin.createMainAgentPlugin("masterAgentPlugin");
        slaveAgentPlugin = createSlaveAgentPlugin("slaveAgentPlugin", "localhost", DEFAULT_CONTAINER_PORT);
        masterAgentPlugin.start();
        slaveAgentPlugin.start();
        assertThat(slaveAgentPlugin.containsContainerProperty(MAIN)).isTrue();
        assertThat((Boolean) slaveAgentPlugin.getContainerProperty(MAIN)).isEqualTo(false);
    }
}