package org.laruche.james.plugin;

import jade.core.AID;
import jade.wrapper.AgentController;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.laruche.james.agent.AbstractAgent;
import org.laruche.james.agent.behavior.AbstractTickingBehavior;
import org.laruche.james.plugin.manager.DefaultPluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static jade.core.Profile.MAIN;
import static jade.core.Profile.PLATFORM_ID;
import static java.lang.Thread.sleep;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.laruche.james.plugin.MainAgentPlugin.createMainAgentPlugin;

class MainAgentPluginTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(MainAgentPluginTest.class);
    private MainAgentPlugin mainAgentPlugin = createMainAgentPlugin("testAgentPlugin");

    /// // Méthode(s) privée(s)

    /**
     * Démarrage du plugin AgentPlugin. <br />
     *
     * @throws Exception : En cas de problème
     */
    private void startAgentPlugin() throws Exception {
        mainAgentPlugin.start();
        sleep(1300);
    }

    /// // Initialisation :

    @AfterEach
    void tearDown() throws Exception {
        LOGGER.info("AgentPluginTest ---> clôture du test");
        if (mainAgentPlugin.isStarted()) {
            mainAgentPlugin.stop();
        }
        mainAgentPlugin.clearAgents();
    }

    /// // tests unitaires :

    @Test
    public void shouldGetId() {
        mainAgentPlugin = createMainAgentPlugin("fredAgentPlugin");
        assertThat(mainAgentPlugin.getId()).isEqualTo("fredAgentPlugin");
    }

    @Test
    public void shouldGetTheParameters() throws Exception {
        mainAgentPlugin.start();
        assertThat(mainAgentPlugin.containsContainerProperty(MAIN)).isTrue();
        assertThat(mainAgentPlugin.containsContainerProperty(PLATFORM_ID)).isTrue();
    }

    @Test
    void shouldStartAgentsFromTheClass() throws Exception {
        mainAgentPlugin.start();
        mainAgentPlugin.addAgentToStart("testAgent1", TestAgent.class);
        mainAgentPlugin.addAgentToStart("testAgent2", TestAgent.class);
        assertThat(mainAgentPlugin.isStarted()).isTrue();
        AgentController agentController = mainAgentPlugin.getAgentController("testAgent1");
        assertThat(agentController).isNotNull();
        agentController = mainAgentPlugin.getAgentController("notExisting");
        assertThat(agentController).isNull();
    }

    @Test
    public void shouldGetAgentController() throws Exception {
        final TestAgent testAgent = new TestAgent();
        mainAgentPlugin.addAgentToStart("testAgent", testAgent);
        startAgentPlugin();
        final AID aid = testAgent.getAID();
        assertThat(aid != null).isTrue();
        assertThat(aid.getLocalName()).isEqualTo("testAgent");
        assertThat(mainAgentPlugin.getAgentController(aid.getLocalName())).isNotNull();
        assertThat(mainAgentPlugin.getAgentController(aid.getName(), true)).isNotNull();
    }


    @Test
    void shouldStartAgentsByInstance() throws Exception {
        final TestAgent testAgent = new TestAgent();
        mainAgentPlugin.addAgentToStart("testAgent", testAgent);
        startAgentPlugin();
        assertThat(mainAgentPlugin.isStarted()).isTrue();
        assertThat(testAgent.isStarted()).isTrue();
        assertThat(mainAgentPlugin.getAgentController("testAgent")).isNotNull();
    }

    @Test
    public void shouldSetPluginManagerWithAgents() {
        final TestAgent agent1 = new TestAgent();
        assertThat(agent1.getPluginManager()).isNull();
        mainAgentPlugin.addAgentToStart("agent1", agent1);
        final TestAgent agent2 = new TestAgent();
        assertThat(agent2.getPluginManager()).isNull();
        mainAgentPlugin.addAgentToStart("agent2", agent2);

        // On initialise le "pluginManager"
        final PluginManager pluginManager = new DefaultPluginManager();
        pluginManager.addPlugin(mainAgentPlugin);
        assertThat(mainAgentPlugin.getPluginManager()).isEqualTo(pluginManager);
        assertThat(agent1.getPluginManager()).isEqualTo(pluginManager);
        assertThat(agent2.getPluginManager()).isEqualTo(pluginManager);
    }

    @Test
    public void shouldThrowExceptionIfCannotStart() throws Exception {

        // Ajouts des différents agents
        final FailStartingAgent failStartingAgent = new FailStartingAgent();
        failStartingAgent.addBehaviour(new TestBehavior());
        mainAgentPlugin.addAgentToStart("failingAgent", failStartingAgent);
        final TestAgent testAgent = new TestAgent();
        testAgent.addBehaviour(new TestBehavior());
        mainAgentPlugin.addAgentToStart("testAgent", testAgent);
        try {
            startAgentPlugin();
            fail("Dpoit envoyer une exception");
        } catch (final Exception e) {
            assertThat(e).isNotNull();
            assertThat(e.getMessage()).isEqualTo("Un des agents n'a pas pu démarré dû à des problèmes d'initialisation");
        }
    }

    /// // Classe(s) Interne(s) :

    private static class TestBehavior extends AbstractTickingBehavior {

        public TestBehavior() {
            super(500L);
        }

        @Override
        protected void doClickAction() {
            LOGGER.info("--> En cours d'execution !!");
        }
    }

    private static class TestAgent extends AbstractAgent {

        public TestAgent() {
            super();
        }

        @Override
        protected void doSetUp() throws Exception {
            super.doSetUp();
        }

        @Override
        protected void doTakeDown() throws Exception {
            super.doTakeDown();
        }
    }

    private static class FailStartingAgent extends AbstractAgent {

        public FailStartingAgent() {
            super();
        }

        @Override
        protected void doSetUp() throws Exception {
            throw new Exception("Cet agent doit échouer au démarrage");
        }
    }
}