package org.laruche.james.plugin;

import jade.core.AID;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.laruche.james.agent.TestSendingMessageAgent;
import org.laruche.james.agent.behavior.AbstractOneShotBehavior;
import org.laruche.james.test.agent.SimpleTestAgent;
import org.laruche.james.test.agent.TestManagerAgent;
import org.laruche.james.test.bean.TestResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static jade.core.AID.ISGUID;
import static jade.lang.acl.ACLMessage.*;
import static java.lang.Thread.sleep;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.assertj.core.api.Assertions.assertThat;
import static org.laruche.james.agent.AgentUtils.createServiceDescription;
import static org.laruche.james.plugin.AbstractAgentPlugin.DEFAULT_CONTAINER_PORT;
import static org.laruche.james.plugin.MainAgentPlugin.createMainAgentPlugin;
import static org.laruche.james.plugin.SlaveAgentPlugin.createSlaveAgentPlugin;

/**
 * Cette classe permet de testsr
 */
public class MultipleAgentPluginTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(MultipleAgentPluginTest.class);
    private final MainAgentPlugin masterAgentPlugin = createMainAgentPlugin("masterAgentPlugin");
    private final SlaveAgentPlugin slaveAgentPlugin = createSlaveAgentPlugin("slaveAgentPlugin", "localhost", DEFAULT_CONTAINER_PORT);

    /// // Initialisation et fermeture

    @AfterEach
    void tearDown() throws Exception {
        if (slaveAgentPlugin.isStarted()) {
            slaveAgentPlugin.stop();
        }
        slaveAgentPlugin.clearAgents();
        if (masterAgentPlugin.isStarted()) {
            masterAgentPlugin.stop();
        }
        masterAgentPlugin.clearAgents();
    }

    /// // Tests unitaires

    @Test
    public void shouldUseMultiContainers() throws Exception {

        // Initialisation de la plateforme maître - esclave
        TestManagerAgent<String> testManagerAgent = new TestManagerAgent<>();
        masterAgentPlugin.addAgentToStart("receivingMessageAgent", testManagerAgent);
        final TestSendingMessageAgent testSendingMessageAgent = new TestSendingMessageAgent();
        testSendingMessageAgent.addMessageToSend("receivingMessageAgent", REQUEST, "Ceci est un test");
        slaveAgentPlugin.addAgentToStart("sendingMessageAgent", testSendingMessageAgent);

        // Démarrage des plugids
        masterAgentPlugin.start();
        slaveAgentPlugin.start();

        // Tests
        sleep(500);
        final TestResult<String> testResult = testManagerAgent.getTestResult();
        assertThat(testResult.isInError()).isFalse();
        assertThat(testResult.getValue()).isEqualTo("Ceci est un test");
    }

    @Test
    public void shouldSlavedAgentFindOthers() throws Exception {

        // Initialisation de la plateforme maître - esclave
        final TestManagerAgent<String> testManagerAgent = new TestManagerAgent<>();
        testManagerAgent.addServiceDescription(createServiceDescription("manageing-agent-service", "manager"));
        masterAgentPlugin.addAgentToStart("masterAgent", testManagerAgent);
        final SimpleTestAgent slaveAgent = new SimpleTestAgent();
        slaveAgent.addBehaviour(new FindAgentBehavior(new AID("masterAgent@masterAgentPlugin", ISGUID), "manageing-agent-service"));
        slaveAgentPlugin.addAgentToStart("slaveAgent", slaveAgent);

        // Démarrage des plateformes
        this.masterAgentPlugin.start();
        this.slaveAgentPlugin.start();

        // Vérification
        sleep(500);
        final TestResult<String> testResult = testManagerAgent.getTestResult();
        assertThat(testResult.isInError()).isFalse();
        assertThat(testResult.getValue()).isEqualTo("Nbre agents = 1");
    }

    @Test
    public void shouldSlavedAgentFindAllAgents() throws Exception {

        // Initialisation de la plateforme maître - esclave
        final TestManagerAgent<String> testManagerAgent = new TestManagerAgent<>();
        testManagerAgent.addServiceDescription(createServiceDescription("manageing-agent-service", "manager"));
        masterAgentPlugin.addAgentToStart("masterAgent", testManagerAgent);
        final SimpleTestAgent slaveAgent = new SimpleTestAgent();
        slaveAgent.addBehaviour(new FindAgentBehavior(new AID("masterAgent@masterAgentPlugin", ISGUID), null));
        slaveAgentPlugin.addAgentToStart("slaveAgent", slaveAgent);

        // Démarrage des plateformes
        this.masterAgentPlugin.start();
        this.slaveAgentPlugin.start();

        // Vérification
        sleep(500);
        final TestResult<String> testResult = testManagerAgent.getTestResult();
        assertThat(testResult.isInError()).isFalse();
        assertThat(testResult.getValue()).isEqualTo("Nbre agents = 2");
    }

    /// // Classe(s) interne(s)

    public static class FindAgentBehavior extends AbstractOneShotBehavior {
        private final AID receiverId;
        private final String serviceName;

        public FindAgentBehavior(final AID receiverId, final String serviceName1) {
            this.receiverId = receiverId;
            serviceName = serviceName1;
        }

        @Override
        protected void doOneShotAction() {
            try {
                final List<DFAgentDescription> agentDescriptions;
                if (!isEmpty(serviceName)) {
                    final DFAgentDescription dfAgentDescription = new DFAgentDescription();
                    dfAgentDescription.addServices(createServiceDescription(serviceName));
                    agentDescriptions = this.searchAgentDescriptions(dfAgentDescription);
                } else {
                    agentDescriptions = this.searchAllAgentDescriptions();
                }
                this.sendMessage(this.receiverId, INFORM, "Nbre agents = %d".formatted(agentDescriptions.size()));
            } catch (final Exception exception) {
                LOGGER.error(exception.getMessage(), exception);
                this.sendMessage(this.receiverId, FAILURE, exception.getMessage());
            }
        }
    }


}
