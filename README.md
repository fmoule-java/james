# James
[_TOC_]
## Introduction
James est projet dont l'objectif est le développement d'une plateforme minimale permettant
de développer des application sur la base de la programmartion multi-agent.
## Installation
### Pré-requis
Pour le développement de ce framework, les briques suivantes sont nécessaires :
* Java (version >= 22)
* Maven

### Récupération et paramétrage du projet
#### De Gitlab
 * Pour la récupération du projet depuis le dépot Gitlab, il suffit de lancer la commande suivante : ```git clone git@gitlab.com:fmoule/james.git```
#### De Github
 * Pour la récupération du projet depuis le dépot Github, il suffit de lancer la commande suivante : ```git clone git@github.com:fmoule/james.git```
### Compilation du projet
 * Se placer dans le dossier : cd james
 * Lancer la compilation : ```mvn clean install```
## Usage
### Création de la plateforme
Pour la creation de la plateforme, il suffit de suivre, créer une classe Java exécutable, 
c'est-à-dire avec une méthode main et d'utiliser la classe <b>PluginManager</b><br />
Puis d'ajouter l'mainAgentPlugin permettant de gérer les agents à démarrer.<br />
Voici un exemple de code Java :
~~~java
public static void main(final String args[]) {
        final PluginManager pluginManager = new PluginManager("pluginManager");
        try {
            pluginManager.addPlugin(createConfigPlugin("configPlugin", args));
            pluginManager.addPlugin(createAgentPlugin("mainAgentPlugin"));
            pluginManager.start();
        } catch (final Exception exception) {
            exception.printStackTrace();
            System.exit(1);
        }
}
~~~
### Typologie d'agent

#### Agent Web
Ce type d'agent a pour objectif d'exposer des API's REST et donc des sites internet. <br />

