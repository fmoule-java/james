package org.laruche.james.plugin.config;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.io.FileUtils.*;
import static org.assertj.core.api.Assertions.assertThat;

@SuppressWarnings({"DataFlowIssue", "CatchMayIgnoreException"})
public class ConfigPluginTest {
    private static final String BASE_PATH = ConfigPluginTest.class.getResource(".").getPath();
    private ConfigPlugin confPlugin;

    /// // Initialisation :

    @AfterEach
    void tearDown() throws Exception {
        if (confPlugin != null && confPlugin.isStarted()) {
            confPlugin.stop();
        }
    }

    /// // Tests unitaires :

    @Test
    public void shouldLoadProperties() throws Exception {
        confPlugin = new ConfigPlugin("confPlugin", BASE_PATH + "/config.properties");
        confPlugin.start();
        assertThat(confPlugin.getNbProperties()).isEqualTo(2);
        assertThat(confPlugin.getProperty("prop1")).isEqualTo("val1");
        assertThat(confPlugin.getProperty("prop2")).isEqualTo("val2");
    }

    @Test
    public void shouldThrowExceptionWithNotExistingFile() {
        confPlugin = new ConfigPlugin("confPlugin", BASE_PATH + "/notExistingFile.properties");
        try {
            confPlugin.start();
        } catch (final Exception exception) {
            assertThat(exception).isNotNull();
            assertThat(exception.getMessage()).isEqualTo("Le fichier de configuration " + BASE_PATH + "/notExistingFile.properties n'existe pas");
        }
    }

    @Test
    public void shouldThrowExceptionWithNullFile() {
        confPlugin = new ConfigPlugin("confPlugin", null);
        try {
            confPlugin.start();
        } catch (final Exception exception) {
            assertThat(exception.getMessage()).isEqualTo("Le fichier de configuration ne doit pas être nul ou vide");
        }
    }

    @Test
    public void shouldWriteProperties() throws Exception {
        final File configFile = new File(BASE_PATH, "testConfig.properties");
        assertThat(configFile.exists()).isFalse();
        try {
            confPlugin = new ConfigPlugin("confPlugin", configFile.getAbsolutePath());
            confPlugin.start();

            // Ajout des propriétés
            confPlugin.addProperty("prop1", "val1");
            confPlugin.addProperty("prop2", "val2");

            // Arrêt du plugin
            confPlugin.stop();
            assertThat(configFile.exists()).isTrue();
            final List<String> lines = readLines(configFile, UTF_8);
            assertThat(lines.size()).isEqualTo(2);
            for (int i = 1; i <= 2; i++) {
                assertThat(lines).contains("prop" + i + "=" + "val" + i);
            }
        } finally {
            deleteQuietly(configFile);
        }
    }

    @Test
    public void shouldWritePropertiesOnExistingFile() throws Exception {
        final File configFile = new File(BASE_PATH, "testConfig.properties");
        assertThat(configFile.exists()).isFalse();
        final List<String> initLines = new ArrayList<>();
        initLines.add("prop1=val1");
        initLines.add("prop2=val2");
        writeLines(configFile, initLines, false);
        try {
            confPlugin = new ConfigPlugin("confPlugin", configFile.getAbsolutePath());
            confPlugin.start();

            // Ajout des propriétés
            confPlugin.addProperty("prop3", "val3");
            confPlugin.addProperty("prop4", "val4");

            // Arrêt du plugin
            confPlugin.stop();
            assertThat(configFile.exists()).isTrue();
            final List<String> lines = readLines(configFile, UTF_8);
            assertThat(lines.size()).isEqualTo(4);
            for (int i = 1; i <= 4; i++) {
                assertThat(lines).contains("prop" + i + "=" + "val" + i);
            }
        } finally {
            deleteQuietly(configFile);
        }
    }

}