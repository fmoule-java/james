package org.laruche.james.plugin;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.laruche.james.plugin.manager.DefaultPluginManager;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.laruche.james.plugin.Plugin.arePluginsStarted;

class AbstractPluginTest {
    private PluginManager pluginManager = new DefaultPluginManager();

    /// / Initialisation

    @AfterEach
    void tearDown() throws Exception {
        this.pluginManager.close();
        this.pluginManager.removeAllPlugins();
    }

    /// // Tests unitaires

    @Test
    public void shouldWaitDependencies() throws Exception {
        final FirstPlugin firstPlugin = new FirstPlugin("plugin1");
        this.pluginManager.addPlugin(firstPlugin);
        final SecundPlugin secundPlugin = new SecundPlugin("plugin2");
        this.pluginManager.addPlugin(secundPlugin);

        // Démarrage :
        this.pluginManager.start();

        // Test
        assertThat(arePluginsStarted(List.of(firstPlugin, secundPlugin))).isTrue();
    }

    /// // Classes internes

    private static class FirstPlugin extends AbstractPlugin {

        public FirstPlugin(final String id) {
            super(id);
        }

        @Override
        protected void doStart() throws Exception {
            Thread.sleep(1000L);
        }

        @Override
        protected void doStop() throws Exception {

        }
    }

    private static class SecundPlugin extends AbstractPlugin {

        public SecundPlugin(final String id) {
            super(id);
            this.addPluginDependency(FirstPlugin.class);
        }

        @Override
        protected void doStart() throws Exception {

        }

        @Override
        protected void doStop() throws Exception {

        }
    }


}