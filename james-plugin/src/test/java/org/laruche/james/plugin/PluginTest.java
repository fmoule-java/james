package org.laruche.james.plugin;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.laruche.james.plugin.Plugin.arePluginsStarted;

class PluginTest {

    /// // Tests unitaires :

    @Test
    public void shouldTestIfStarted() throws Exception {

        final TestPlugin plugin1 = new TestPlugin("plugin1");
        final TestPlugin plugin2 = new TestPlugin("plugin2");

        assertThat(arePluginsStarted(List.of(plugin1, plugin2))).isFalse();
        plugin1.start();
        assertThat(arePluginsStarted(List.of(plugin1, plugin2))).isFalse();
        plugin2.start();
        assertThat(arePluginsStarted(List.of(plugin1, plugin2))).isTrue();
    }

    /// // Classe(s) interne(s)

    private static class TestPlugin extends AbstractPlugin {


        /// // Constructeur(s)

        public TestPlugin(final String id) {
            super(id);
        }

        /// // Méthode(s)

        @Override
        protected void doStart() {
            // Do nathing
        }

        @Override
        protected void doStop() {
            // Do nothing
        }
    }

}