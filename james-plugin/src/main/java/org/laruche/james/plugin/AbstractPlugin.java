package org.laruche.james.plugin;

import java.util.Collection;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Predicate;

import static java.lang.Thread.sleep;
import static org.laruche.james.plugin.Plugin.arePluginsStarted;
import static org.laruche.james.plugin.PluginManager.ClassPredicate.withClass;

/**
 * <p>
 * Classe abstraite représentant un modèle pour tous les plugins utilisés par la plateforme. <br />
 * </p>
 *
 * @author Frédéric Moulé
 * @see Plugin
 */
public abstract class AbstractPlugin implements Plugin {
    private boolean isStarted = false;
    private final String id;
    private final Set<Class<? extends Plugin>> pluginDependencies = new HashSet<>();
    private transient PluginManager pluginManager;
    private final transient ReentrantReadWriteLock lock;

    /// Constructeur(s) :

    protected AbstractPlugin(final String id) {
        this.id = id;
        this.lock = new ReentrantReadWriteLock();
    }

    /// Méthodes de la classe Object :

    @Override
    public int hashCode() {
        return Objects.hashCode(this.id);
    }

    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if ((obj == null) || (this.getClass() != obj.getClass())) {
            return false;
        }
        return Objects.equals(this.id, ((AbstractPlugin) obj).id);
    }

    /// Méthodes de l'interface Plugin :

    @Override
    public void start() throws Exception {
        this.lock.writeLock().lock();
        try {
            waitDependencies();
            this.doStart();
            this.isStarted = true;
        } finally {
            this.lock.writeLock().unlock();
        }
    }

    /**
     * <p>
     * Méthode permettant d'attendre que toutes les dépendances soient
     * bien démamrré
     * </p>
     *
     * @throws TimeoutException : Exception renvoyée lorsque le temps d'attente est dépassé
     */
    protected void waitDependencies() throws Exception {
        if (pluginDependencies.isEmpty() || this.pluginManager == null) {
            return;
        }
        Predicate<Plugin> pluginPredicate = null;
        for (Class<? extends Plugin> pluginDependencyClass : pluginDependencies) {
            if (pluginPredicate == null) {
                pluginPredicate = withClass(pluginDependencyClass);
            } else {
                pluginPredicate = pluginPredicate.and(withClass(pluginDependencyClass));
            }
        }
        final Collection<Plugin> foundPlugins = this.pluginManager.findPlugins(pluginPredicate);
        if (foundPlugins.size() != this.pluginDependencies.size()) {
            throw new Exception("Il manque des plugins dans le plugin manager afin de démarrer le plugin");
        }
        int timeCursor = 0;
        while (!arePluginsStarted(foundPlugins) && timeCursor <= 20) {
            //noinspection BusyWait
            sleep(200L);
            timeCursor++;
        }
        if (!arePluginsStarted(foundPlugins)) {
            throw new TimeoutException("Time out : toutes les dépendances ne sont pos démarrées");
        }
    }

    protected abstract void doStart() throws Exception;

    @Override
    public void stop() throws Exception {
        this.lock.writeLock().lock();
        try {
            this.doStop();
            this.isStarted = false;
        } finally {
            this.lock.writeLock().unlock();
        }
    }

    protected abstract void doStop() throws Exception;


    /// // Méthodes générales :
    public void addPluginDependency(final Class<? extends Plugin> pluginClass) {
        if (pluginClass == null) {
            return;
        }
        this.pluginDependencies.add(pluginClass);
    }

    ///// Getters & Setters :

    /**
     * Permet de retourner un booléen montrant si le plugin est
     * démarré. <br />
     *
     * @return booléen
     */
    @Override
    public boolean isStarted() {
        lock.readLock().lock();
        try {
            return this.isStarted;
        } finally {
            lock.readLock().unlock();
        }
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public Collection<Class<? extends Plugin>> getPluginDependencies() {
        return pluginDependencies;
    }

    public PluginManager getPluginManager() {
        return pluginManager;
    }

    public void setPluginManager(final PluginManager pluginManager) {
        this.pluginManager = pluginManager;
    }
}