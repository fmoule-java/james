package org.laruche.james.plugin;

public interface Identifiable {

    /**
     * Retourne l'id de l'instance courante. <br />
     *
     * @return id
     */
    String getId();
}
