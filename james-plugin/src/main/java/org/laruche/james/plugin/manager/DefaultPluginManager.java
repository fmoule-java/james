package org.laruche.james.plugin.manager;

import org.laruche.james.plugin.AbstractPlugin;
import org.laruche.james.plugin.Plugin;
import org.laruche.james.plugin.PluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.function.Predicate;

import static org.apache.commons.lang3.RandomStringUtils.random;

/**
 * <p>
 * Gestionnaire des plugins
 * </p>
 *
 * @author Frédéric Moulé
 * @see PluginManager
 */
public class DefaultPluginManager extends AbstractPlugin implements PluginManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(DefaultPluginManager.class);
    private static final PluginComparator DEFAULT_PLUGIN_COMPARATOR = new PluginComparator();
    private final Set<Plugin> plugins = new TreeSet<>(DEFAULT_PLUGIN_COMPARATOR);

    ///// Constructeur(s) :

    /**
     * Constructeur avec l'identifiant. <br />
     *
     * @param id : identifiant du manageur.
     */
    public DefaultPluginManager(final String id) {
        super(id);
    }

    /**
     * Constructeur par défaut. <br />
     */
    public DefaultPluginManager() {
        this(random(10));
    }

    /// Méthodes de l'interface Plugin :

    @Override
    protected void doStart() throws Exception {
        for (Plugin plugin : plugins) {
            plugin.start();
        }
    }

    @Override
    protected void doStop() throws Exception {
        LOGGER.info("PluginManager en cours de fermeture");
        for (Plugin plugin : plugins) {
            plugin.stop();
        }
        this.plugins.clear();
        LOGGER.info("PluginManager fermé");
    }

    /// Méthodes générales :

    @Override
    public void addPlugin(final Plugin plugin) {
        if (plugin instanceof AbstractPlugin) {
            ((AbstractPlugin) plugin).setPluginManager(this);
        }
        this.plugins.add(plugin);
    }

    /**
     * Permet de supprimer les plugins validant le prédicat passé
     * en paramètre. <br />
     *
     * @param pluginPredicate : prédicat utilisé pour la sélection.
     * @throws Exception : En cas de problème
     */
    public void removePlugin(final Predicate<? super Plugin> pluginPredicate) throws Exception {
        final Collection<Plugin> foundPlugins = this.findPlugins(pluginPredicate);
        for (Plugin foundPlugin : foundPlugins) {
            if (!foundPlugin.isStarted()) {
                continue;
            }
            foundPlugin.stop();
        }
        this.plugins.removeAll(foundPlugins);
    }

    @Override
    public <T extends Plugin> Collection<T> findPlugins(final Predicate<? super T> predicate) {
        final ArrayList<T> filteredlugins = new ArrayList<>();
        if (predicate == null) {
            return filteredlugins;
        }
        T castPredicate;
        for (Plugin plugin : plugins) {
            try {
                //noinspection unchecked
                castPredicate = (T) plugin;
                if (!predicate.test(castPredicate)) {
                    continue;
                }
                filteredlugins.add(castPredicate);
            } catch (final ClassCastException classCastException) {
                // EMPTY
            }
        }
        return filteredlugins;
    }

    /// // Classes Internes :

    private static class PluginComparator implements Comparator<Plugin> {

        @Override
        public int compare(final Plugin plugin1, final Plugin plugin2) {
            if (plugin1 == null && plugin2 == null) {
                return 0;
            } else if (plugin1 == null) {
                return -1;
            } else if (plugin2 == null) {
                return 1;
            }
            final Collection<Class<? extends Plugin>> collect1 = plugin1.getPluginDependencies();
            final Collection<Class<? extends Plugin>> collect2 = plugin2.getPluginDependencies();
            if (collect1 != null && collect1.contains(plugin2.getClass())) {
                return 1;
            } else if (collect2 != null && collect2.contains(plugin1.getClass())) {
                return -1;
            } else {
                return plugin1.compareTo(plugin2);
            }
        }
    }


}