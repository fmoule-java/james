package org.laruche.james.plugin;

import org.laruche.james.plugin.utils.BeanEqualsPredicate;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.empty;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * <p>
 * Interface définissant les gestionnaires de plugin. <br />
 * </p>
 *
 * @see Plugin
 */
public interface PluginManager extends Plugin {

    void addPlugin(Plugin plugin);

    void removePlugin(final Predicate<? super Plugin> plugin) throws Exception;

    <T extends Plugin> Collection<T> findPlugins(Predicate<? super T> predicate);

    ///// Méthode

    /**
     * <p>
     * Méthode permettant de récupérer le premier plugin
     * vérifiant le prédicat passé en paramètre. <br />
     * </p>
     *
     * @param predicate : prédicat
     * @param <T>       : type du plugin à chercher
     * @return instance du plugin
     */
    default <T extends Plugin> Optional<T> findFirstPlugin(final Predicate<? super T> predicate) {
        if (predicate == null) {
            return empty();
        }
        final Collection<T> foundPlugins = this.findPlugins(predicate);
        if (foundPlugins != null && !foundPlugins.isEmpty()) {
            return foundPlugins.stream().findFirst();
        } else {
            return empty();
        }
    }

    /**
     * <p>
     * Méthode permettant de récupérer le premier plugin dont
     * la classe correspond au paramètre de la fonction.<br />
     * </p>
     *
     * @param pluginClass :  classe du plugin
     * @param <T>         : type de Plugin
     * @return premier plugin de classe <i>pluginClass</i>
     */
    default <T extends Plugin> Optional<T> findFirstPlugin(final Class<? extends T> pluginClass) {
        if (pluginClass == null) {
            return empty();
        }
        return this.findFirstPlugin(new ClassPredicate(pluginClass));
    }


    /**
     * <p>
     * Méthode permettant de récupérer le plugin en fonction de son identifiant. <br />
     * </p>
     *
     * @param pluginId : id du plugin
     * @param <T>      : type du plugin
     * @return instance du plugin
     */
    default <T extends Plugin> Optional<T> findFirstPlugin(final String pluginId) {
        if (isEmpty(pluginId)) {
            return empty();
        }
        return this.findFirstPlugin(new PluginIdPredicate(pluginId));
    }

    /**
     * Méthode permettant de supprimer un plugin particulier. <br />
     *
     * @param plugin : plugin à supprimer
     * @throws Exception :  En cas de problème
     */
    default void removePlugin(final Plugin plugin) throws Exception {
        if (plugin == null) {
            return;
        }
        this.removePlugin(new BeanEqualsPredicate<>(plugin));
    }

    /**
     * Méthode permettant de supprimer tous les plugins. <br />
     *
     * @throws Exception : En cas de problème
     */
    default void removeAllPlugins() throws Exception {
        this.removePlugin(Objects::nonNull);
    }

    ///// Classes internes :

    /**
     * <p>
     * Prédicat testant la classe de l'objet. <br />
     * </p>
     */
    class ClassPredicate implements Predicate<Plugin> {
        private final Class<? extends Plugin> initClass;

        public ClassPredicate(final Class<? extends Plugin> pluginClass) {
            this.initClass = requireNonNull(pluginClass, "La classe passé au constructeur ne doit pas être nulle");
        }

        @Override
        public boolean test(final Plugin plugin) {
            if (plugin == null) {
                return false;
            }
            return initClass.equals(plugin.getClass());
        }


        public static Predicate<Plugin> withClass(final Class<? extends Plugin> pluginClass) {
            return new ClassPredicate(pluginClass);
        }
    }

    class PluginIdPredicate implements Predicate<Plugin> {
        private final String pluginId;

        public PluginIdPredicate(final String pluginId) {
            this.pluginId = pluginId;
        }

        @Override
        public boolean test(final Plugin plugin) {
            if (plugin == null) {
                return false;
            }
            return pluginId.equals(plugin.getId());
        }
    }
}
