package org.laruche.james.plugin.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.function.Predicate;

/**
 * <p>
 * Prédicat permettant de tester si un objet appartient à une collection donnée. <br />
 * </p>
 *
 * @param <T> : type des objets concernés
 * @author Frédéric Moulé
 * @see Predicate
 */
public class BeanInCollectionPredicate<T> implements Predicate<T> {
    private final Collection<T> beans = new ArrayList<>();

    public BeanInCollectionPredicate(final Collection<? extends T> beans) {
        this.beans.addAll(beans);
    }

    @Override
    public boolean test(final T bean) {
        return this.beans.contains(bean);
    }
}
