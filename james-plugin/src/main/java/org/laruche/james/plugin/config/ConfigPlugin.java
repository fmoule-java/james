package org.laruche.james.plugin.config;

import org.laruche.james.plugin.AbstractPlugin;

import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static org.apache.commons.io.FileUtils.writeLines;
import static org.apache.commons.lang3.RandomStringUtils.random;
import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * <p>
 * Plugin type permettant de gérer et d'importer un fichier de configuration.<br />
 * De façon plus précise, ce plugin importe le fichier de configuration voulu et expose
 * les propriétés. <br />
 * </p>
 *
 * @see Properties
 */
public class ConfigPlugin extends AbstractPlugin {
    private final transient Properties confProperties = new Properties();
    private final String confFilePath;

    /// // Constructeur(s) :

    public ConfigPlugin(final String id, final String confFilePath) {
        super(id);
        this.confFilePath = confFilePath;
    }

    /**
     * Constructeur avec le chemin du fichier de configuration. <br />
     *
     * @param confFilePath : chemin du fichier de configuration
     */
    public ConfigPlugin(final String confFilePath) {
        this(random(10), confFilePath);
    }

    /// // Méthode

    @Override
    protected void doStart() throws Exception {
        if (isEmpty(confFilePath)) {
            throw new Exception("Le fichier de configuration ne doit pas être nul ou vide");
        }
        final File confFile = new File(confFilePath);
        if (confFile.exists() && confFile.isDirectory()) {
            throw new Exception("Le fichier " + confFilePath + " est un dossier et no pas un fichier");
        } else if (confFile.exists() && confFile.isFile()) {
            this.confProperties.load(new FileReader(confFile));
        }
    }

    @Override
    protected void doStop() throws Exception {

        // Sauvegarde des propriétés
        final File configFile = new File(confFilePath);
        if (configFile.exists() && configFile.isDirectory()) {
            throw new Exception("Le chemin dans le fichier de configuation correspond à un dossier !!");
        }

        // Ecriture
        final List<Object> lines = new ArrayList<>();
        Object propValue;
        for (Object propName : confProperties.keySet()) {
            propValue = confProperties.get(propName);
            if (propName == null || propValue == null) {
                continue;
            }
            lines.add(propName + "=" + propValue);
        }
        writeLines(configFile, lines, false);

        // Nettoyage
        this.confProperties.clear();
    }

    /// // Méthode(s) générale(s)

    /**
     * Ajout d'une propriété dans le plugin. <br />
     *
     * @param key   : nom de la propriété
     * @param value :  valeur de la propriète
     */
    public void addProperty(final String key, final String value) {
        this.confProperties.setProperty(key, value);
    }

    /// // Getters & Setters :

    public String getProperty(final String propName) {
        if (isEmpty(propName)) {
            return null;
        }
        return this.confProperties.getProperty(propName);
    }

    public int getNbProperties() {
        return this.confProperties.size();
    }

}
