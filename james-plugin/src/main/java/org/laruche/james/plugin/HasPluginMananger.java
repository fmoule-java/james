package org.laruche.james.plugin;

public interface HasPluginMananger {

    /**
     * Retourne l'instance PluginManager
     *
     * @return instance
     * @see PluginManager
     */
    PluginManager getPluginManager();

    /**
     * Initialise le manageur de plugins;
     *
     * @param pluginManager : instance du manager de plugins
     */
    void setPluginManager(PluginManager pluginManager);
}
