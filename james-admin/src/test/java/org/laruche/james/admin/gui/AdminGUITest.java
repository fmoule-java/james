package org.laruche.james.admin.gui;

import org.junit.jupiter.api.Test;
import org.laruche.james.agent.javafx.JavaFXApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.laruche.james.agent.javafx.JavaFXApplication.INIT_FXML_FILE_NAME;

@SuppressWarnings("DataFlowIssue")
class AdminGUITest {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminGUITest.class);
    private static final String BASE_PATH = AdminGUITest.class.getResource("/").getPath();

    /// // Tests unitaires

    @Test
    public void shouldTestIfFXMLFileExists() {
        final File fxmlFile = new File(BASE_PATH, INIT_FXML_FILE_NAME);
        assertThat(fxmlFile.exists()).isTrue();
    }

    @Test
    public void shoudGetInitFXML() throws FileNotFoundException {
        final JavaFXApplication javaFXApplication = new JavaFXApplication();
        final File initFxmlFile = javaFXApplication.getInitFxmlFile();
        assertThat(initFxmlFile).isNotNull();
        assertThat(initFxmlFile.exists()).isTrue();
    }

    /// // Test à la main

    /*
    @Disabled("A lancer à la main")
    public static void main(final String[] args) {
        try (PluginManager pluginManager = new DefaultPluginManager()) {
            final File fxmlFile = new File(BASE_PATH, INIT_FXML_FILE_NAME);
            if (!fxmlFile.exists()) {
                throw new FileNotFoundException("Fichier FXML n'existe pas : %s".formatted(fxmlFile.getAbsolutePath()));
            }
            final AgentPlugin agentPlugin = new AgentPlugin("testAgentPlugin");
            agentPlugin.addAgentToStart("guiAgent", getJavaFXAgentInstance());
            pluginManager.addPlugin(agentPlugin);
            pluginManager.start();
            while (pluginManager.isStarted()) {
                LOGGER.debug("--> En cours d'execution");
                sleep(500);
            }
            System.exit(0);
        } catch (final Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
            System.exit(1);
        }
    }*/
}