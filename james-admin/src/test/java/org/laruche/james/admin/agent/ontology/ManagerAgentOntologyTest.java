package org.laruche.james.admin.agent.ontology;

import jade.content.abs.AbsObject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.laruche.james.admin.agent.ontology.ListAgentAction.ListAgentActionResponse;
import org.laruche.james.agent.TestSendingMessageAgent;
import org.laruche.james.test.AbstractAgentTestCase;
import org.laruche.james.test.agent.TestManagerAgent;
import org.laruche.james.test.bean.TestResult;

import static jade.lang.acl.ACLMessage.INFORM;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.laruche.james.admin.agent.ontology.ManagerAgentOntology.MANAGER_AGENT_ONTOLOGY;
import static org.laruche.james.agent.behavior.AbstractHandlingMessageBehavior.HandlingMessageMode.OBJECT;

class ManagerAgentOntologyTest extends AbstractAgentTestCase<ListAgentActionResponse> {
    private TestSendingMessageAgent testSendingMessageAgent;

    /// // Initialisation :

    @Override
    @BeforeEach
    public void setUp() {

        // Défintion des agents :
        this.testManagerAgent = new TestManagerAgent<>(OBJECT);
        this.testSendingMessageAgent = new TestSendingMessageAgent();

        // Ajout des agents :
        this.mainAgentPlugin.addAgentToStart(TEST_MANAGER_AGENT_NAME, this.testManagerAgent);
        this.mainAgentPlugin.addAgentToStart("sendingMessageAgent", this.testSendingMessageAgent);
    }

    @Override
    @AfterEach
    public void tearDown() throws Exception {
        super.tearDown();
    }

    /// // Tests unitaires :

    @Test
    public void shouldConvertFrom() throws Exception {
        final AbsObject absObject = MANAGER_AGENT_ONTOLOGY.fromObject(new ListAgentAction());
        assertThat(absObject).isNotNull();
    }

    @Test
    public void shouldReceiveMessageWithOntology() throws Exception {

        // Création du contenu de message :
        final ListAgentActionResponse messageContent = new ListAgentActionResponse();
        messageContent.setAgentNames(asList("Agent1", "Agent2"));

        // Définition de l'agent qui enverra
        testSendingMessageAgent.addMessageToSend(TEST_MANAGER_AGENT_NAME, INFORM, MANAGER_AGENT_ONTOLOGY, messageContent);
        this.startAgentPlugin();

        // Récupération du résultat
        final TestResult<ListAgentActionResponse> testResult = this.testManagerAgent.getTestResult();
        assertThat(testResult).isNotNull();
        assertThat(testResult.isInError()).isFalse();

        // Vérification du résultat
        final ListAgentActionResponse listAgentActionResponse = testResult.getValue();
        assertThat(listAgentActionResponse).isNotNull();
        assertThat(listAgentActionResponse.getAgentNames()).isEqualTo(asList("Agent1", "Agent2"));
    }

}