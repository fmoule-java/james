package org.laruche.james.admin.agent;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.laruche.james.admin.agent.ontology.ListAgentAction;
import org.laruche.james.admin.agent.ontology.ListAgentAction.ListAgentActionResponse;
import org.laruche.james.agent.TestSendingMessageAgent.TestSendingMessageBehavior;
import org.laruche.james.agent.behavior.AbstractTickingBehavior;
import org.laruche.james.test.AbstractAgentTestCase;
import org.laruche.james.test.agent.SimpleTestAgent;
import org.laruche.james.test.agent.TestManagerAgent;
import org.laruche.james.test.bean.TestResult;

import java.io.Serializable;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;
import static org.laruche.james.admin.agent.ontology.ManagerAgentOntology.MANAGER_AGENT_ONTOLOGY;
import static org.laruche.james.agent.AgentUtils.createServiceDescription;
import static org.laruche.james.agent.behavior.AbstractHandlingMessageBehavior.HandlingMessageMode.OBJECT;
import static org.laruche.james.message.MessagePerformative.REQUEST;


class ManagerAgentTest extends AbstractAgentTestCase<Serializable> {
    private ManagerAgent managerAgent;

    /// // Initialisation :

    @Override
    @BeforeEach
    public void setUp() {

        // Ajout de l'ontologie
        this.testManagerAgent = new TestManagerAgent<>(OBJECT);
        this.testManagerAgent.addOntology(MANAGER_AGENT_ONTOLOGY);
        this.testManagerAgent.addServiceDescription(createServiceDescription("testing-service", "test"));

        // Définition des agents :
        this.managerAgent = new ManagerAgent();

        // Ajout des agents pour les tests
        this.mainAgentPlugin.addAgentToStart(TEST_MANAGER_AGENT_NAME, this.testManagerAgent);
        this.mainAgentPlugin.addAgentToStart("managerAgent", managerAgent);
    }

    @Override
    @AfterEach
    public void tearDown() throws Exception {
        super.tearDown();
    }

    /// // Tests unitaires :

    @Test
    public void shouldGetTheAgentName() throws Exception {

        // Démarrage
        this.startAgentPlugin();

        // Test du nom :
        assertThat(managerAgent).isNotNull();
        assertThat(managerAgent.getName()).isNotNull();
        assertThat(managerAgent.getName()).isNotEmpty();
    }

    @Test
    public void shouldGetAgentList() throws Exception {

        // Ajout du comportement
        this.testManagerAgent.addBehaviour(new TestSendingMessageBehavior("managerAgent", REQUEST, new ListAgentAction()));

        // Démarrage
        this.startAgentPlugin();

        // Récupération du résultat :
        final TestResult<Serializable> testResult = this.testManagerAgent.getTestResult();
        if (testResult.isInError()) {
            fail("Résultat en erreur : %s".formatted(testResult.getErrorMessage()));
        }
        assertThat(testResult.isInError()).isFalse();
        final Serializable respContent = testResult.getValue();
        assertThat(respContent).isNotNull();
        if (respContent instanceof ListAgentActionResponse listAgentActionResponse) {
            final List<String> agentNames = listAgentActionResponse.getAgentNames();
            assertThat(agentNames).isNotNull();
            assertThat(agentNames).isNotEmpty();
            assertThat(agentNames.size()).isGreaterThanOrEqualTo(2);
        } else {
            fail("La réponse doit implémenter ListAgentActionResponse");
        }
    }

    @Test
    public void shouldListAgentsWithoutDescription() throws Exception {

        // Ajout du comportement
        this.testManagerAgent.addBehaviour(new TestSendingMessageBehavior("managerAgent", REQUEST, new ListAgentAction()));

        // Ajout du nouvel agent
        final SimpleTestAgent testAgent = new SimpleTestAgent();
        testAgent.addBehaviour(new AbstractTickingBehavior(500L) {
            @Override
            protected void doClickAction() {
                this.getLogger().info("----> SimpleTestAgent : Click");
            }
        });
        this.mainAgentPlugin.addAgentToStart("simpleAgent", testAgent);
        this.startAgentPlugin();

        // Récupération du résultat :
        Thread.sleep(1000L);
        final TestResult<Serializable> testResult = this.testManagerAgent.getTestResult();
        assertThat(testResult).isNotNull();
        if (testResult.isInError()) {
            fail("TestResult in error : " + testResult.getErrorMessage());
        }
        assertThat(testResult.isInError()).isFalse();
        final Serializable respContent = testResult.getValue();
        assertThat(respContent).isNotNull();
        if (respContent instanceof ListAgentActionResponse listAgentActionResponse) {
            final List<String> agentNames = listAgentActionResponse.getAgentNames();
            assertThat(agentNames).isNotNull();
            assertThat(agentNames).isNotEmpty();
            assertThat(agentNames.size()).isGreaterThanOrEqualTo(3);
        } else {
            fail("La réponse doit implémenter ListAgentActionResponse");
        }

    }

}