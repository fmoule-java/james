package org.laruche.james.admin.gui.controllers;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import org.laruche.james.agent.javafx.AbstractJavaFXController;
import org.laruche.james.plugin.PluginManager;

@SuppressWarnings("unused")
public class MainAdminGUIController extends AbstractJavaFXController {

    @FXML
    private Button closeButton;

    /// // Méthode(s)

    @Override
    public void initialize() {
        super.initialize();
    }

    /// // Evènements

    @FXML
    public void close(final Event event)  {
        if (this.javaFXAgent == null) {
            return;
        }
        final PluginManager pluginManager = getPluginManager();
        try {
            pluginManager.close();
        } catch (final Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
            throw new RuntimeException(exception);
        }
    }
}
