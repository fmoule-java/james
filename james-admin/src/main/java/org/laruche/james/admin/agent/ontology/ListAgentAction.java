package org.laruche.james.admin.agent.ontology;

import jade.content.AgentAction;
import jade.content.Concept;
import jade.content.onto.annotations.AggregateSlot;
import jade.content.onto.annotations.Element;

import java.util.*;

/**
 * <p>
 * Action de l'ontologie correspondant à l'action listant les agents
 * de la plateforme. <br />
 * </p>
 *
 * @author Frédéric Moulé
 */
@Element(name = "listAction")
public class ListAgentAction implements AgentAction {


    /// // Classe(s) interne(s) :

    @Element(name = "listActionResponse")
    public static class ListAgentActionResponse implements Concept {
        private Set<String> agentNames;

        /// // Constructeur(s) :

        /**
         * Constructeur de base
         */
        public ListAgentActionResponse() {
            this.agentNames = new HashSet<>();
        }

        /// // Méthode(s) générale(s) :

        public void addAgentName(final String agentName) {
            this.agentNames.add(agentName);
        }

        /// // Getters & Setters :

        @AggregateSlot(type = String.class)
        public List<String> getAgentNames() {
            return new ArrayList<>(agentNames);
        }

        public void setAgentNames(final Collection<String> agentNames) {
            this.agentNames = (agentNames == null ? new HashSet<>() : new HashSet<>(agentNames));
        }
    }

}
