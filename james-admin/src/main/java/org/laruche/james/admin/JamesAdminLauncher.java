package org.laruche.james.admin;

import org.laruche.james.plugin.PluginManager;
import org.laruche.james.plugin.config.ConfigPlugin;
import org.laruche.james.plugin.manager.DefaultPluginManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static java.lang.Thread.sleep;
import static org.apache.commons.lang3.RandomStringUtils.random;
import static org.apache.commons.lang3.StringUtils.isEmpty;

public class JamesAdminLauncher {
    private static final Logger LOGGER = LoggerFactory.getLogger(JamesAdminLauncher.class);

    /// // Méthode(s) privée(s) :

    private static ConfigPlugin initConfigPlugin(final String[] args) throws Exception {
        if (args == null || args.length == 0) {
            throw new Exception("Pas d'arguments passés en paramètres");
        }
        final String configFilePath = args[0];
        if (isEmpty(configFilePath)) {
            throw new Exception("Le chemin du fichier de configuration est vide !!");
        }
        return new ConfigPlugin("configPlugin-" + random(10), configFilePath);
    }

    ///// Méthodes MAIN :

    /**
     * Lancement de la console administration
     *
     * @param args : arguments
     */
    public static void main(final String[] args) {
        try (final PluginManager pluginManager = new DefaultPluginManager("pluginManager-" + random(10))) {

            // Ajout des plugins nécessaires :
            pluginManager.addPlugin(initConfigPlugin(args));

            // Démarrage des plug-ins :
            LOGGER.info("==> Démarrage de James admin");
            pluginManager.start();
            while (pluginManager.isStarted()) {
                //noinspection BusyWait
                sleep(500);
            }
            LOGGER.info("==> Arrêt de James admin");
            System.exit(0);
        } catch (final Exception exception) {
            LOGGER.error(exception.getMessage(), exception);
            System.exit(1);
        }
    }
}