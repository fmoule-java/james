package org.laruche.james.admin.agent.ontology;

import jade.content.onto.BeanOntology;
import jade.content.onto.BeanOntologyException;

/**
 * <p>
 * Onotologie utilisée par le <b>ManagerAgent</b>
 * </p>
 *
 * @author Frédéric Moulé
 */
public class ManagerAgentOntology extends BeanOntology {
    public static final String MANAGER_AGENT_ONTOLOGY_NAME = "managerAgentOntology";
    public static final ManagerAgentOntology MANAGER_AGENT_ONTOLOGY;

    static {
        try {
            MANAGER_AGENT_ONTOLOGY = new ManagerAgentOntology();
        } catch (final BeanOntologyException beanOntologyException) {
            throw new RuntimeException("Impossible de créer l'ontologie ManagerAgentOntology");
        }
    }

    /// // Constructeur(s) :

    private ManagerAgentOntology() throws BeanOntologyException {
        super(MANAGER_AGENT_ONTOLOGY_NAME);

        // Ajout des actions :
        this.add(ListAgentAction.class);
    }

}
