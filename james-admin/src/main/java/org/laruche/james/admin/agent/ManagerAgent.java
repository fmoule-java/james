package org.laruche.james.admin.agent;

import jade.content.AgentAction;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.lang.acl.ACLMessage;
import org.laruche.james.admin.agent.ontology.ListAgentAction;
import org.laruche.james.admin.agent.ontology.ListAgentAction.ListAgentActionResponse;
import org.laruche.james.agent.AbstractAgent;
import org.laruche.james.agent.behavior.AbstractHandlingMessageBehavior;

import static jade.lang.acl.ACLMessage.INFORM;
import static jade.lang.acl.ACLMessage.REQUEST;
import static jade.lang.acl.MessageTemplate.MatchPerformative;
import static jade.lang.acl.MessageTemplate.or;
import static org.laruche.james.admin.agent.ontology.ManagerAgentOntology.MANAGER_AGENT_ONTOLOGY;
import static org.laruche.james.agent.AgentUtils.createServiceDescription;

/**
 * <p>
 * Agent manager permettant de gérer les agents de la plateforme. <br />
 * </p>
 *
 * @author Frédéric Moulé
 * @see AbstractAgent
 * @see org.laruche.james.plugin.PluginManager
 */
public class ManagerAgent extends AbstractAgent {

    /// // Contructeur(s) :

    /**
     * Constructeur de l'agent.
     */
    public ManagerAgent() {

        // Définition des paramètres de l'agent :
        this.addOntology(MANAGER_AGENT_ONTOLOGY);
        this.addServiceDescription(createServiceDescription("managing-service", "manager"));

        // Ajout des comportements :
        this.addBehaviour(new ManagerHandlingMessageBehavior());
    }

    /// // Initialisation de l'agent

    @Override
    protected void doSetUp() throws Exception {
        super.doSetUp();
    }

    /// // Classe(s) interne(s) :

    private static class ManagerHandlingMessageBehavior extends AbstractHandlingMessageBehavior {

        /// // Constructeur(s) :

        public ManagerHandlingMessageBehavior() {
            super(or(MatchPerformative(INFORM), MatchPerformative(REQUEST)));
        }

        /// // Action de la classe AbstractHandlingMessageBehavior

        @Override
        public void doAction(final ACLMessage message) {
            try {
                final AgentAction agentAction = this.extractAgentActionFromMessage(message);
                if (agentAction instanceof ListAgentAction) {

                    // Construction de la réponse :
                    final ListAgentActionResponse listAgentActionResponse = new ListAgentActionResponse();

                    // Récupération de l'instance DFSService
                    for (DFAgentDescription dfAgentDescription : searchAllAgentDescriptions()) {
                        if (dfAgentDescription == null || dfAgentDescription.getName() == null) {
                            continue;
                        }
                        listAgentActionResponse.addAgentName(dfAgentDescription.getName().getName());
                    }

                    // Envoi de la réponse
                    this.sendMessage(message.getSender(), INFORM, MANAGER_AGENT_ONTOLOGY, listAgentActionResponse);
                }
            } catch (final Exception exception) {
                this.sendFailureResponse(message, exception);
            }
        }

    }

}
